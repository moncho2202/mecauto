<?php

namespace Application\Sonata\UserBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Entity\UserAdmin as BaseUserAdmin;

class UserAdmin extends BaseUserAdmin
{

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // Get user company
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        
        // define group zoning
        $formMapper
            ->tab('User')
                ->with('General', array('class' => 'col-md-6'))->end()
            ->end()
            ->tab('Security')
                ->with('Status', array('class' => 'col-md-4'))->end()
                ->with('Groups', array('class' => 'col-md-4'))->end()
                ->with('Roles', array('class' => 'col-md-12'))->end()
            ->end()
        ;

        $now = new \DateTime();

        if (!$company){
            $formMapper
                ->tab('User')
                    ->with('General')
                        ->add('username')
                        ->add('email')
                        ->add('plainPassword', 'text', array(
                            'required' => (!$this->getSubject() || is_null($this->getSubject()->getId())),
                        ))
                        ->add('company', null, ['label' => 'Empresa'])
                    ->end()
                ->end()
           ;
        }else{
            $formMapper
                ->tab('User')
                    ->with('General')
                        ->add('username')
                        ->add('email')
                        ->add('plainPassword', 'text', array(
                            'required' => (!$this->getSubject() || is_null($this->getSubject()->getId())),
                        ))
                        ->add('company', 'entity', 
                                    [
                                        'label' => 'Empresa',
                                        'class' => 'AppBundle\Entity\Company',
                                        'query_builder' => function($er) use ($company){
                                            return $er->createQueryBuilder('c')
                                                    ->where('c.id = :company')
                                                    ->setParameter('company', $company);
                                        }, 
                                        'property' => null,
                                    ]
                                )
                    ->end()
                ->end()
           ;
        }
                
        if ($this->getSubject() && !$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->tab('Security')
                    ->with('Status')
                        ->add('locked', null, array('required' => false))
                        ->add('expired', null, array('required' => false))
                        ->add('enabled', null, array('required' => false))
                        ->add('credentialsExpired', null, array('required' => false))
                    ->end()
                    ->with('Groups')
                        ->add('groups', 'sonata_type_model', array(
                            'required' => false,
                            'expanded' => true,
                            'multiple' => true,
                        ))
                    ->end()
                    ->with('Roles')
                        ->add('realRoles', 'sonata_security_roles', array(
                            'label'    => 'form.label_roles',
                            'expanded' => true,
                            'multiple' => true,
                            'required' => false,
                        ))
                    ->end()
                ->end()
            ;
        }
    }

    public function createQuery($context = 'list')
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();

        $query = parent::createQuery($context);
        if ($company && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $query->andWhere($query->getRootAliases()[0] . '.company = :company');
            $query->setParameter('company', $company);
        }
        return $query;
    }
}

