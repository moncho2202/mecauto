<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\File;

class FileController extends Controller
{
    public function fileAction(File $file)
    {
        return new Response($file->getFileAsBin(), 200,
            array('Content-Type' => $file->getMime())
        );
    }
}
