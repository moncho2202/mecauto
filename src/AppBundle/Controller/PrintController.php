<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\OrderEntry;
use AppBundle\Entity\OrderRepair;
use AppBundle\Entity\Budget;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceCash;
use AppBundle\Entity\InvoiceReceipt;
use AppBundle\Entity\InvoiceRefund;
use AppBundle\Entity\InvoiceCashRefund;

class PrintController extends Controller
{
    public function printOrderEntryAction(Request $request, OrderEntry $oe)
    {
        $this->denyAccessUnlessGranted('view', $oe);
        return $this->render('AppBundle:Admin:printOrderEntry.html.twig', ['oe' => $oe]);
    }
    
    public function printOrderRepairAction(Request $request, OrderEntry $oe, OrderRepair $or)
    {
        $this->denyAccessUnlessGranted('view', $oe);
        return $this->render('AppBundle:Admin:printOrderRepair.html.twig', ['oe' => $oe, 'or' => $or]);
    }

    public function printBudgetAction(Request $request, OrderEntry $oe, Budget $budget)
    {
        $this->denyAccessUnlessGranted('view', $oe);
        return $this->render('AppBundle:Admin:printBudget.html.twig', ['oe' => $oe, 'budget' => $budget]);
    }

    public function printInvoiceAction(Request $request, OrderEntry $oe, Invoice $invoice)
    {
        $this->denyAccessUnlessGranted('view', $oe);
        return $this->render('AppBundle:Admin:printInvoice.html.twig', ['oe' => $oe, 'invoice' => $invoice]);
    }

    public function printInvoiceCashAction(Request $request, InvoiceCash $invoice)
    {
        $this->denyAccessUnlessGranted('view', $invoice);
        return $this->render('AppBundle:Admin:printInvoiceCash.html.twig', ['invoice' => $invoice]);
    }

    public function printInvoiceReceiptAction(Request $request, OrderEntry $oe, InvoiceReceipt $receipt)
    {
        $this->denyAccessUnlessGranted('view', $oe);
        return $this->render('AppBundle:Admin:printInvoiceReceipt.html.twig', ['oe' => $oe, 'receipt' => $receipt]);
    }

    public function printRefundAction(Request $request, InvoiceRefund $refund)
    {
        $oe = $refund->getInvoice()->getOe();
        $this->denyAccessUnlessGranted('view', $oe);
        return $this->render('AppBundle:Admin:printInvoiceRefund.html.twig', ['oe' => $oe, 'invoice' => $refund]);
    }

    public function printCashRefundAction(Request $request, InvoiceCashRefund $refund)
    {
        $this->denyAccessUnlessGranted('view', $refund);
        return $this->render('AppBundle:Admin:printInvoiceCashRefund.html.twig', ['invoice' => $refund]);
    }
}
