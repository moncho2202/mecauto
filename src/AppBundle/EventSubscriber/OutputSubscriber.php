<?php

namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

use AppBundle\Entity\InvoiceItem;
use AppBundle\Entity\Output;
use AppBundle\Entity\Part;

Class OutputSubscriber implements EventSubscriber
{            
    public function getSubscribedEvents()
    {
        return [
            'prePersist',            
            'onFlush',
        ];
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        
        // Creating invoice add output
        if ($entity instanceof InvoiceItem) {
            if ($entity->getItem() instanceof Part){
                $this->addOutput($entity);
            }
        }
    } 

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            // Updating invoice item add output
            if ($entity instanceof InvoiceItem && $entity->getItem() instanceof Part) {
                $output = $entity->getOutput();
                if ($output){
                    $output->setAmount(-$entity->getAmount());
                }else{
                    $output = $this->addOutput($entity);
                    $uow->persist($output);
                }
                $uow->computeChangeSet($em->getClassMetadata(get_class($output)), $output);
            }
        }
    }

    /**
     * Add output
     * 
     * @param InvoiceItem $entity
     * @return Output
     */
    private function addOutput(InvoiceItem $entity)
    {
        $output = new Output();
        $output->setInvoiceItem($entity);
        $output->setPart($entity->getItem());
        $output->setAmount(-$entity->getAmount());
        $entity->setOutput($output);
        
        return $output;
    }
}
