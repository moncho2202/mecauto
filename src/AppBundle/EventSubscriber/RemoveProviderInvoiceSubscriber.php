<?php

namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;

use AppBundle\Entity\ProviderInvoice;

Class RemoveProviderInvoiceSubscriber implements EventSubscriber
{            
    public function getSubscribedEvents()
    {
        return [           
            'onFlush',
        ];
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            // Deleting provider invoice
            if ($entity instanceof ProviderInvoice) {
                $query = $em->createQuery('UPDATE AppBundle:DeliveryNote a '
                        . 'SET a.invoice = null '
                        . 'WHERE a.invoice = :invoice')
                        ->setParameter('invoice', $entity);
                $query->execute();
            }
        }
    }
}
