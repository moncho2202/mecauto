<?php

namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

use AppBundle\Entity\DeliveryNotePart;
use AppBundle\Entity\OrderEntry;
use AppBundle\Entity\BudgetItem;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceRefund;
use AppBundle\Entity\InvoiceCash;
use AppBundle\Entity\InvoiceCashRefund;
use AppBundle\Entity\InvoiceItem;
use AppBundle\Entity\Labor;

Class DefaultValuesSubscriber implements EventSubscriber
{            
    public function getSubscribedEvents()
    {
        return [
            'prePersist',            
        ];
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        
        // Creating delivery note add input
        if ($entity instanceof DeliveryNotePart) {
            $this->setDeliveryNotePartDefaultValues($entity);
        }

        // Creating oe
        if ($entity instanceof OrderEntry) {
            $this->setOeDefaultValues($entity);
        }

        // Creating budget item
        if ($entity instanceof BudgetItem) {
            $this->setBudgetItemDefaultValues($entity);
        }
       
        // Creating invoice
        if ($entity instanceof Invoice) {
            $this->setInvoiceDefaultValues($entity);
        }

        // Creating invoice
        if ($entity instanceof InvoiceCash) {
            $this->setInvoiceCashDefaultValues($entity);
        }

        // Creating invoice refund
        if ($entity instanceof InvoiceRefund) {
            $this->setInvoiceRefundDefaultValues($entity);
        }

        // Creating invoice refund
        if ($entity instanceof InvoiceCashRefund) {
            $this->setInvoiceCashRefundDefaultValues($entity);
        }

        // Creating invoice item
        if ($entity instanceof InvoiceItem) {
            $this->setInvoiceItemDefaultValues($entity);
        }
    } 

    private function setDeliveryNotePartDefaultValues(DeliveryNotePart $entity)
    {
        $part = $entity->getPart();
        is_null($entity->getDiscount())?$entity->setDiscount($part->getDiscount()):null;
        is_null($entity->getTax())?$entity->setTax($part->getTax()):null;
        is_null($entity->getPrice())?$entity->setPrice($part->getPrice()):null;
    }

    private function setOeDefaultValues(OrderEntry $entity)
    {
        $customer = new \AppBundle\Entity\CustomerData($entity->getCustomer());
        $entity->setCustomerData($customer);

        $vehicle = new \AppBundle\Entity\VehicleData($entity->getVehicle());
        $entity->setVehicleData($vehicle);
        if ($entity->getAddress()){
            $entity->setAddressData($entity->getAddress()->getAddr());
        }
    }

    private function setBudgetItemDefaultValues(BudgetItem $entity)
    {
        $item = $entity->getItem();
        
        is_null($entity->getDescription())?$entity->setDescription($item->getDescription()):null;
        is_null($entity->getAmount())?$entity->setAmount($item->getAmount()):null;
        is_null($entity->getDiscount())?$entity->setDiscount($item->getDiscount()):null;
        is_null($entity->getTax())?$entity->setTax($item->getTax()):null;
        if ($item instanceof Labor && $item->getUnit() == 'HOUR'){
            is_null($entity->getPrice())?$entity->setPrice($entity->getBudget()->getOe()->getCompany()->getDefaultPriceByHour()):null;
        }else{
            is_null($entity->getPrice())?$entity->setPrice($item->getPrice()):null;
        }
    }

    private function setInvoiceDefaultValues(Invoice $entity)
    {
        if ($entity->getCustomer()){
            $customer = new \AppBundle\Entity\CustomerData($entity->getCustomer());
            $entity->setCustomerData($customer);
            if ($entity->getAddress()){
                $entity->setAddressData($entity->getAddress()->getAddr());
            }
        }else{ 
            // Fijar antes la dirección que el cliente porque sino se fija la dirección por defecto
            $entity->setAddress($entity->getOe()->getAddress());
            $entity->setAddressData($entity->getOe()->getAddressData());
            
            $customer = new \AppBundle\Entity\CustomerData($entity->getOe()->getCustomer());
            $entity->setCustomer($entity->getOe()->getCustomer());
            $entity->setCustomerData($customer);
        }

        $vehicle = new \AppBundle\Entity\VehicleData($entity->getOe()->getVehicle());
        $entity->setVehicle($entity->getOe()->getVehicle());
        $entity->setVehicleData($vehicle);
    }

    private function setInvoiceCashDefaultValues(InvoiceCash $entity)
    {
        $customer = new \AppBundle\Entity\CustomerData($entity->getCustomer());
        $entity->setCustomerData($customer);
        if ($entity->getAddress()){
            $entity->setAddressData($entity->getAddress()->getAddr());
        }
    }
    
    private function setInvoiceRefundDefaultValues(InvoiceRefund $entity)
    {
        $customer = new \AppBundle\Entity\CustomerData($entity->getCustomer());
        $entity->setCustomerData($customer);
        if ($entity->getAddress()){
            $entity->setAddressData($entity->getAddress()->getAddr());
        }
    }

    private function setInvoiceCashRefundDefaultValues(InvoiceCashRefund $entity)
    {
        $customer = new \AppBundle\Entity\CustomerData($entity->getCustomer());
        $entity->setCustomerData($customer);
        if ($entity->getAddress()){
            $entity->setAddressData($entity->getAddress()->getAddr());
        }
    }

    private function setInvoiceItemDefaultValues(InvoiceItem $entity)
    {
        $item = $entity->getItem();
        
        is_null($entity->getDescription())?$entity->setDescription($item->getDescription()):null;
        is_null($entity->getAmount())?$entity->setAmount($item->getAmount()):null;
        is_null($entity->getDiscount())?$entity->setDiscount($item->getDiscount()):null;
        is_null($entity->getTax())?$entity->setTax($item->getTax()):null;
        if ($item instanceof Labor && $item->getUnit() == 'HOUR'){
            is_null($entity->getPrice())?$entity->setPrice($entity->getInvoice()->getOe()->getCompany()->getDefaultPriceByHour()):null;
        }else{
            is_null($entity->getPrice())?$entity->setPrice($item->getPrice()):null;
        }
    }
}
