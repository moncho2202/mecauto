<?php

namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceItem;
use AppBundle\Entity\InvoiceReceipt;
use AppBundle\Entity\Labor;

Class ExcessSubscriber implements EventSubscriber
{            
    public function getSubscribedEvents()
    {
        return [
            'prePersist',               
        ];
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
       
        // Creating invoice
        if ($entity instanceof Invoice) {
            if ($entity->getExcess() > 0){
                $this->createReceipt($entity);
            }
        }

        // Creating invoice item
        if ($entity instanceof InvoiceItem) {
            if ($entity->getItem() instanceof Labor && $entity->getItem()->getType()->getExcess() 
                    && !$entity->getInvoice()->getChild() && !$entity->getInvoice()->getId()){
                $this->createInvoice($entity);
            }
        }
    }
    
    private function createReceipt(Invoice $entity)
    {
        $receipt = new InvoiceReceipt();
        $receipt->setDescription("Franquicia de la factura Nº: {$entity->getNumber()}");
        $receipt->setAmount($entity->getExcess());
        $receipt->setCustomer($entity->getOe()->getCustomerData());
        $receipt->setAddress($entity->getOe()->getAddressData());
        $receipt->setVehicle($entity->getOe()->getVehicleData());
        $receipt->setInvoice($entity);
        $entity->setReceipt($receipt);
        
        return $receipt;
    }

    private function createInvoice(InvoiceItem $entity)
    {
        $invoice = new Invoice();
        $item = clone $entity;
        $item->setPrice(-$item->getPrice());
        $invoice->setOe($entity->getInvoice()->getOe());
        $invoice->setBudget($entity->getInvoice()->getBudget());
        $invoice->addItem($item);
        $invoice->setInvoice($entity->getInvoice());
        $entity->getInvoice()->getOe()->addInvoice($invoice);
        
        return $invoice;
    }
}
