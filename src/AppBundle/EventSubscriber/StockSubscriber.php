<?php

namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

use AppBundle\Entity\Input;
use AppBundle\Entity\Output;

Class StockSubscriber implements EventSubscriber
{            
    public function getSubscribedEvents()
    {
        return [
            'onFlush',            
        ];
    }
    
    public function onFlush(OnFlushEventArgs $eventArgs)
    {        
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            // Creating input, output update stock
            if ($entity instanceof Input || $entity instanceof Output) {
                $entity->getPart()->setStock($entity->getPart()->getStock()+$entity->getAmount());
                $class = $em->getClassMetadata(get_class($entity->getPart()));
                $em->getUnitOfWork()->computeChangeSet($class, $entity->getPart());
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            // Updating input, output update stock
            if ($entity instanceof Input || $entity instanceof Output) {
                $changes = $uow->getEntityChangeSet($entity);
                if (!empty($changes['amount'] && $changes['amount'][0] != $changes['amount'][1])){
                    $entity->getPart()->setStock($entity->getPart()->getStock()+($changes['amount'][1] - $changes['amount'][0]));
                    $class = $em->getClassMetadata(get_class($entity->getPart()));
                    $em->getUnitOfWork()->computeChangeSet($class, $entity->getPart());
                }
            }
        }
        
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            // Deleting input, output update stock
            if ($entity instanceof Input || $entity instanceof Output) {
                $entity->getPart()->setStock($entity->getPart()->getStock()-$entity->getAmount());
                $class = $em->getClassMetadata(get_class($entity->getPart()));
                $em->getUnitOfWork()->computeChangeSet($class, $entity->getPart());
            }
        }
    } 
}
