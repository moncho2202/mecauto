<?php

namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

use AppBundle\Entity\OrderEntry;
use AppBundle\Entity\OrderRepair;
use AppBundle\Entity\Budget;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceCash;
use AppBundle\Entity\InvoiceRefund;
use AppBundle\Entity\InvoiceCashRefund;

Class ConsecutiveNumbersSubscriber implements EventSubscriber
{
    private $last = null;
    
    public function getSubscribedEvents()
    {
        return [
            'prePersist',            
        ];
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        
        // Generating OE number
        if ($entity instanceof OrderEntry) {
            $this->generateNumber($em, $entity, $entity->getCompany(), 'OrderEntry', 'setNumberOe', 'getNumberOe', $entity->getDateIn() ? $entity->getDateIn()->format('Y') : null);
        }
        
        // Generating OR number
        if ($entity instanceof OrderRepair) {
            $this->generateNumber($em, $entity, $entity->getOE()->getCompany(), 'OrderRepair', 'setNumberOr', 'getNumberOr', $entity->getDate() ? $entity->getDate()->format('Y') : null);
        }
        
        // Generating Budget number
        if ($entity instanceof Budget) {
            $this->generateNumber($em, $entity, $entity->getOE()->getCompany(), 'Budget', 'setNumberBudget', 'getNumberBudget', $entity->getDate() ? $entity->getDate()->format('Y') : null);
        }

        // Generating Invoice number
        if ($entity instanceof Invoice) {
            $this->generateNumber($em, $entity, $entity->getOE()->getCompany(), 'Invoice', 'setNumberInvoice', 'getNumberInvoice', $entity->getDate() ? $entity->getDate()->format('Y') : null);
        }

        // Generating Invoice number
        if ($entity instanceof InvoiceCash) {
            $this->generateNumber($em, $entity, $entity->getCompany(), 'InvoiceCash', 'setNumberInvoice', 'getNumberInvoice', $entity->getDate() ? $entity->getDate()->format('Y') : null);
        }

        // Generating Refund number
        if ($entity instanceof InvoiceRefund) {
            $this->generateNumber($em, $entity, $entity->getCompany(), 'InvoiceRefund', 'setNumberInvoiceRefund', 'getNumberInvoiceRefund', $entity->getDate() ? $entity->getDate()->format('Y') : null);
        }

        // Generating Refund number
        if ($entity instanceof InvoiceCashRefund) {
            $this->generateNumber($em, $entity, $entity->getCompany(), 'InvoiceCashRefund', 'setNumberInvoiceRefund', 'getNumberInvoiceRefund', $entity->getDate() ? $entity->getDate()->format('Y') : null);
        }
    }

    private function generateNumber($em, $entity, $company, $entityName, $setNumberName, $getNumberName, $year)
    {
        $numbers = $em->getRepository('AppBundle:Numbers')->findOneByCompany($company);
        if (!$numbers || !$numbers->$getNumberName()){
            $maxNumber = $em->getRepository("AppBundle:$entityName")->findMaxNumberByCompany($company, $year)+1;
            $maxNumber = (isset($this->last[$entityName]) && ($this->last[$entityName] == $maxNumber))?($maxNumber+1):$maxNumber;
        }else{
            $maxNumber = $numbers->$getNumberName();
            $numbers->$setNumberName(null);
            $class = $em->getClassMetadata(get_class($numbers));
            $em->getUnitOfWork()->computeChangeSet($class, $numbers); 
        }
        $entity->setSequence($maxNumber);
        $this->last[$entityName] = $maxNumber;
    }
}
