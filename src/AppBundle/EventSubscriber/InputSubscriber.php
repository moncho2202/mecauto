<?php

namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

use AppBundle\Entity\DeliveryNotePart;
use AppBundle\Entity\Input;

Class InputSubscriber implements EventSubscriber
{            
    public function getSubscribedEvents()
    {
        return [
            'prePersist',            
            'onFlush',
        ];
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        
        // Creating delivery note add input
        if ($entity instanceof DeliveryNotePart) {
            $this->addInput($entity);
        }
    } 

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            // Updating delivery note add input
            if ($entity instanceof DeliveryNotePart) {
                $input = $entity->getInput();
                if ($input){
                    $input->setAmount($entity->getAmount());
                }else{
                    $input = $this->addInput($entity);
                    $uow->persist($input);
                }
                $uow->computeChangeSet($em->getClassMetadata(get_class($input)), $input);
            }
        }
    }

    /**
     * Add input
     * 
     * @param DeliveryNotePart $entity
     * @return Input
     */
    private function addInput(DeliveryNotePart $entity)
    {
        $input= new Input();
        $input->setDeliveryNotePart($entity);
        $input->setPart($entity->getPart());
        $input->setAmount($entity->getAmount());
        $entity->setInput($input);

        return $input;
    }
}
