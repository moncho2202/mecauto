<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCompanyData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        $companyMain = $this->createCompanyMain();
        $companySecond = $this->createCompanySecond();
        
        $manager->persist($companyMain);
        $manager->persist($companySecond);
        $manager->flush();
        
        $this->addReference('company-main', $companyMain);
        $this->addReference('company-second', $companySecond);
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
    
    private function createCompanyMain()
    {
        $company = new \AppBundle\Entity\Company();
        $company->setAlias('principal');
        $company->setDefaultPriceByHour(30);
        $company->setDefaultTax(0.21);
        $company->setEmail('talleres@frugar.es');
        $company->setIdentifier('B81631723');
        $company->setMovil(null);
        $company->setName('Talleres Frugar S.L.');
        $company->setPhone('918414627');
        $company->setRegistered('Inscrita en Registro Merc. de Madrid, Tomo 11858, Folio 105, Sección 8A, Libro 0, Hoja M-186235');
        $company->setRegistry('CM-14793');
        $company->setPrefixOe('OE');
        $company->setPrefixOr('OR');
        $company->setPrefixBudget('P');
        $company->setPrefixInvoice('F');
        $company->setPrefixInvoiceRefund('FA');
        $addr = new \AppBundle\Entity\AddressData();
        $addr->setCity('VALDETORRES DE JARAMA');
        $addr->setCode('28150');
        $addr->setState('Madrid');
        $addr->setStreet('Calle arroyo de la poza Nº 16 Polígono Industrial Valtoron Nave 18');
        $company->setAddr($addr);
        
        return $company;
    }
    
    private function createCompanySecond()
    {
        $company = new \AppBundle\Entity\Company();
        $company->setAlias('secundaria');
        $company->setDefaultPriceByHour(30);
        $company->setDefaultTax(0);
        $company->setEmail('talleres@frugar.es');
        $company->setIdentifier('B81631723');
        $company->setMovil(null);
        $company->setName('Talleres Frugar S.L.');
        $company->setPhone('918414627');
        $company->setRegistered('Inscrita en Registro Merc. de Madrid, Tomo 11858, Folio 105, Sección 8A, Libro 0, Hoja M-186235');
        $company->setRegistry('CM-14793');
        $company->setPrefixOe('SOE');
        $company->setPrefixOr('SOR');
        $company->setPrefixBudget('SP');
        $company->setPrefixInvoice('SF');
        $company->setPrefixInvoiceRefund('SFA');
        $addr = new \AppBundle\Entity\AddressData();
        $addr->setCity('VALDETORRES DE JARAMA');
        $addr->setCode('28150');
        $addr->setState('Madrid');
        $addr->setStreet('Calle arroyo de la poza Nº 16 Polígono Industrial Valtoron Nave 18');
        $company->setAddr($addr);
        
        return $company;
    }
}
