<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPartData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i<=200; $i++){
            $part = $this->createPart($i);
            $manager->persist($part);
        }

        $manager->flush();
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 9;
    }

    private function createPart($i)
    {
        if ($i%3==0){
            $provider = $this->getReference('provider-second');
        }else{
            $provider = $this->getReference('provider-main');
        }
        
        $part = new \AppBundle\Entity\Part();
        $part->setDescription("PIEZA PRUEBA $i");
        $part->setDiscount($i/521);
        $part->setPrice(($i*99)+(($i*99)*21/100));
        $part->setProvider($provider);
        $part->setRef("REF".str_pad($i, 4, "0000", STR_PAD_LEFT));
        $part->setTax(21/100);
        $part->setTrademark("MARCA $i");
        $part->setVehicle("VEHICULO $i");
        
        return $part;
    }
}
