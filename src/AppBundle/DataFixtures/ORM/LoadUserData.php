<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadSuperUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $this->createUserMain();
        $this->createUserSecond();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 4;
    }
    
    private function createUserMain()
    {
        $userManager = $this->container->get('fos_user.user_manager');
        
        // Insertamos usuario principal
        $user = $userManager->createUser();
        $user->setUsername('frugar');
        $user->setEmail('raul@frugar.es');
        $user->setPlainPassword('frugar');
        //$user->setPassword('3NCRYPT3D-V3R51ON');
        $user->setEnabled(true);
        $user->addGroup($this->getReference('group-company-admin'));
        $user->setCompany($this->getReference('company-main'));
        $userManager->updateUser($user, true);
    }
    
    private function createUserSecond()
    {
        $userManager = $this->container->get('fos_user.user_manager');
        
        // Insertamos usuario secundario
        $user = $userManager->createUser();
        $user->setUsername('tfrugar');
        $user->setEmail('carlos@frugar.es');
        $user->setPlainPassword('tfrugar');
        //$user->setPassword('3NCRYPT3D-V3R51ON');
        $user->setEnabled(true);
        $user->addGroup($this->getReference('group-company-admin'));
        $user->setCompany($this->getReference('company-second'));
        $userManager->updateUser($user, true);
    }
}
