<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLaborTypeData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        $laborType = new \AppBundle\Entity\LaborType();
        $laborType->setName('Mécanica');
        $laborType->setDescription('Mécanica');
        $laborType->setExcess(false); 
        $laborType->setActive(true);   
        $manager->persist($laborType);
        $this->addReference('labortype-mec', $laborType);
        
        $laborType = new \AppBundle\Entity\LaborType();
        $laborType->setName('Chapa y Pintura');
        $laborType->setDescription('Chapa y Pintura');
        $laborType->setExcess(false); 
        $laborType->setActive(true);        
        $manager->persist($laborType);
        $this->addReference('labortype-pin', $laborType);

        $laborType = new \AppBundle\Entity\LaborType();
        $laborType->setName('Franquicia');
        $laborType->setDescription('Franquicia');
        $laborType->setActive(true);        
        $laborType->setExcess(true);        
        $manager->persist($laborType);
        $this->addReference('labortype-fra', $laborType);
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 5;
    }
}
