<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProviderData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        $providerMain = $this->createProviderMain();
        $providerSecond = $this->createProviderSecond();
        
        $manager->persist($providerMain);
        $manager->persist($providerSecond);
        $manager->flush();
        
        $this->addReference('provider-main', $providerMain);
        $this->addReference('provider-second', $providerSecond);
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 8;
    }
    
    private function createProviderMain()
    {
        $provider = new \AppBundle\Entity\Provider();
        $provider->setAlias('Principal');
        $provider->setEmail('proveedor@principal.com');
        $provider->setIdentifier('00000000T');
        $provider->setMovil('611111111');
        $provider->setName('Proveedor 1');
        $provider->setPhone('911111111');
        $addr = new \AppBundle\Entity\AddressData();
        $addr->setCity('Madrid');
        $addr->setCode('28001');
        $addr->setState('Madrid');
        $addr->setStreet('Calle real Nº 16 Polígono Industrial Nave 1');
        $provider->setAddr($addr);
        
        return $provider;
    }
    
    private function createProviderSecond()
    {
         $provider = new \AppBundle\Entity\Provider();
        $provider->setAlias('Secundario');
        $provider->setEmail('proveedor@secundario.com');
        $provider->setIdentifier('00000001R');
        $provider->setMovil('622222222');
        $provider->setName('Proveedor 2');
        $provider->setPhone('922222222');
        $addr = new \AppBundle\Entity\AddressData();
        $addr->setCity('Madrid');
        $addr->setCode('28002');
        $addr->setState('Madrid');
        $addr->setStreet('Calle nueva Nº 16 Polígono Industrial Nave 2');
        $provider->setAddr($addr);
        
        return $provider;
    }
}
