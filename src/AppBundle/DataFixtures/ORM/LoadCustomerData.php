<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCustomerData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i<=100; $i++){
            $customer = $this->createCustomer($i);
            $vehicle = $this->createVehicle($i, $customer);
            
            $manager->persist($customer);
            $manager->persist($vehicle);
        }

        $manager->flush();
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 6;
    }
    
    private function createCustomer($i)
    {
        $customer = new \AppBundle\Entity\Customer();
        $customer->setEmail("prueba$i@prueba.com");
        $number = str_pad($i, 8, "00000000", STR_PAD_LEFT);
        $nif = $number.substr('TRWAGMYFPDXBNJZSQVHLCKE', $number % 23, 1);
        $customer->setIdentifier($nif);
        $customer->setMovil("6".str_pad($i, 8, "00000000", STR_PAD_LEFT));
        $customer->setName("nombre$i");
        $customer->setPhone("9".str_pad($i, 8, "00000000", STR_PAD_LEFT));
        $customer->setSurname1("primer$i");
        $customer->setSurname2("prueba$i");
        $addr = new \AppBundle\Entity\AddressData();
        $addr->setCity("Madrid");
        $addr->setCode('28'.  str_pad($i, 3, "000", STR_PAD_LEFT));
        $addr->setState('Madrid');
        $addr->setStreet("Calle de prueba, $i");
        $address = new \AppBundle\Entity\Address();
        $address->setAddr($addr);
        $address->setDefault(true);
        $address->setCustomer($customer);
        $customer->addAddres($address);
        
        return $customer;
    }
    
    private function createVehicle($i, $customer)
    {
        $vehicle = new \AppBundle\Entity\Vehicle();
        $vehicle->setColour("COLOR P$i");
        $vehicle->setColourCode("CHR$i");
        $vehicle->setEngine("MOTOR P$i");
        $vehicle->setModel("PRUEBA$i");
        $vehicle->setVin("FFFVIN$i");
        $vehicle->setRegistration(str_pad($i, 4, "0000", STR_PAD_LEFT)."ABC");
        $vehicle->setTrademark("MARCA$i");
        $vehicle->setCustomer($customer);
        
        return $vehicle;
    }
}
