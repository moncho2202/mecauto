<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDeliveryNoteData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i<=100; $i++){
            $deliveryNote = $this->createDeliveryNote($i, $manager);
            $manager->persist($deliveryNote);
        }

        $manager->flush();
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 10;
    }
    
    private function createDeliveryNote($i, $manager)
    {
        $deliveryNote = new \AppBundle\Entity\DeliveryNote();
        
        $deliveryNote->setDate(new \DateTime("now -$i days"));
        if ($i%9==0){
            $cod = 'S';
            $provider = $this->getReference('provider-second');
        }else{
            $cod = 'P';
            $provider = $this->getReference('provider-main');
        }
        $deliveryNote->setProvider($provider);
        $deliveryNote->setNumber(str_pad($i, 4, "0000", STR_PAD_LEFT));
        for ($j=1; $j<=(($i%3)+1); $j++){
            $parts = $manager->getRepository("AppBundle:Part")->findBy(['provider' => $provider]);
            $pos = ($i*$j) % count($parts);
            $part = $parts[$pos];
            $deliveryNotePart = new \AppBundle\Entity\DeliveryNotePart();
            $deliveryNotePart->setAmount(($i%4)+1);
            $deliveryNotePart->setPart($part);
            $deliveryNote->addPart($deliveryNotePart);
        }
        
        return $deliveryNote;
    }
}
