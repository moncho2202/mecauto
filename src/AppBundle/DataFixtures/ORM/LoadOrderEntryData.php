<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadOrderEntryData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i<=100; $i++){
            $this->createOrderEntry($i, $manager);
        }
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 11;
    }
    
    private function createOrderEntry($i, $manager)
    {
        $oe = new \AppBundle\Entity\OrderEntry();
        $oe->setDateIn(new \DateTime("now -$i days"));
        if ($i%9==0){
            $company = $this->getReference('company-second');
        }else{
            $company = $this->getReference('company-main');
        }
        $oe->setCompany($company);
        $customer = $manager->getRepository("AppBundle:Customer")->findAll()[$i];
        $oe->setCustomer($customer);
        $oe->setVehicle($customer->getVehicles()[0]);
        $oe->setAddress($customer->getAddressDefault());
        $oe->setDeposit($i/100);
        $oe->setKm($i+10000);
        $oe->setDescription('Order de entrada '.$i);
        $manager->persist($oe);
        
        $budget = new \AppBundle\Entity\Budget();
        $budget->setDate(new \DateTime("now -$i days"));
        $budget->setOe($oe);
        for ($j=1; $j<=(($i%3)+1); $j++){
            $items = $manager->getRepository("AppBundle:Item")->findAll();
            $pos = ($i*$j) % count($items);
            $item = $items[$pos];
            $budgetItem = new \AppBundle\Entity\BudgetItem();
            $budgetItem->setItem($item);
            $budget->addItem($budgetItem);
        }
        $manager->persist($budget);
        
        $or = new \AppBundle\Entity\OrderRepair();
        $or->copyBudget($budget);
        $manager->persist($or);

        $invoice = new \AppBundle\Entity\Invoice();
        $invoice->copyBudget($budget);
        $manager->persist($invoice);
        
        // Hay que hacer el flush por linea para que se genere correctamente los numeros de OR
        $manager->flush();
    }
}
