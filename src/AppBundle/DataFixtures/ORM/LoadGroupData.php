<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $groupManager = $this->container->get('fos_user.group_manager');
        
        // Insertamos grupo admin de empresa
        $group = $groupManager->createGroup('Administrador de Empresa');
        $group->setRoles($this->getRolesAdminCompany());
        $groupManager->updateGroup($group, true);
        
        $this->addReference('group-company-admin', $group);
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 3;
    }
    
    /**
     * Devuelve todos los roles de sonata admin
     * 
     * @return array
     */
    private function getRoles()
    {
        $pool = $this->container->get('sonata.admin.pool');

        // get roles from the Admin classes
        foreach ($pool->getAdminServiceIds() as $id) {
            try {
                $admin = $pool->getInstance($id);
            } catch (\Exception $e) {
                continue;
            }
            
            $baseRole = $admin->getSecurityHandler()->getBaseRole($admin);

            if (strlen($baseRole) == 0) { // the security handler related to the admin does not provide a valid string
                continue;
            }

            foreach ($admin->getSecurityInformation() as $role => $permissions) {
                $role = sprintf($baseRole, $role);
                $roles[$role] = $role;
            }
        }
        
        return $roles;
    }
    
    /**
     * Devuelve todos los roles para el grupo administrador de empresa
     * 
     * @return array
     */
    private function getRolesAdminCompany()
    {
        $roles = $this->getRoles();
        
        //Quitamos los roles no necesarios
        unset($roles['ROLE_SONATA_ADMIN_COMPANY_CREATE']);
        unset($roles['ROLE_SONATA_ADMIN_COMPANY_DELETE']);
        unset($roles['ROLE_SONATA_ADMIN_COMPANY_EXPORT']);
        unset($roles['ROLE_SONATA_ADMIN_COMPANY_OPERATOR']);
        unset($roles['ROLE_SONATA_ADMIN_COMPANY_MASTER']);
        
        //Ponemos los roles fuera de sonata admin
        $roles['ROLE_ADMIN: ROLE_USER, ROLE_SONATA_ADMIN'] = 'ROLE_ADMIN';
        
        return $roles;
    }
}
