<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLaborData extends AbstractFixture implements OrderedFixtureInterface
{    
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=101; $i++){
            $labor = $this->createLabor($i);
            $manager->persist($labor);
        }

        $manager->flush();
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 7;
    }
    
    private function createLabor($i)
    {
        $labor = new \AppBundle\Entity\Labor();
        
        $labor->setDiscount($i/521);
        if ($i%3==0){
            $labor->setHour($i);
        }else{
            $labor->setPrice($i);
        }
        $labor->setTax(21/100);
        if ($i%2==0){
            $labor->setDescription("MO Mecánica Prueba $i");
            $labor->setRef("MOMEC".str_pad($i, 4, "0000", STR_PAD_LEFT));
            $type = $this->getReference('labortype-mec');
        }else{
            $labor->setDescription("MO Pintura Prueba $i");
            $labor->setRef("MOPIN".str_pad($i, 4, "0000", STR_PAD_LEFT));
            $type = $this->getReference('labortype-pin');
        }
        $labor->setType($type);
        
        return $labor;
    }
}
