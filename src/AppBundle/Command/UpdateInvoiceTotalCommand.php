<?php /** @noinspection PhpUnused */

namespace AppBundle\Command;

use AppBundle\Entity\Bill;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateInvoiceTotalCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('app:invoice-total:update')

            // the short description shown while running "php app/console list"
            ->setDescription('Actualiza el campo total de la tabla de facturas.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Actualiza el campo total de la tabla de facturas...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var Bill[] $invoices */
        $invoices = $em->getRepository("AppBundle:Bill")->findAll();

        foreach ($invoices as $invoice) {
            try {
                $em->getConnection()->executeUpdate(
                    "UPDATE bill SET total = '" . $invoice->getTotal() ."' WHERE id = " . $invoice->getId()
                );

                $output->writeln('InvoiceId '.$invoice->getId().' <info>'.$invoice->getTotal().'</info>');
            } catch (DBALException $e) {
                $output->writeln('InvoiceId '.$invoice->getId().' <error>'.$e->getMessage().'</error>');
            }
        }
    }
}