<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class ProviderInvoiceAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->modelManager->getEntityManager('AppBundle\Entity\DeliveryNote');
        $query = $em->createQueryBuilder('a')
                    ->select('a')
                    ->from('AppBundle:DeliveryNote', 'a')
                    ->where('a.invoice is null'
                            .((!empty($this->getSubject()->getId()))?' OR a.invoice = '.$this->getSubject()->getId():''))
                    ->orderBy('a.id', 'DESC');
        if (!empty($this->getSubject()->getProvider()) &&
                !empty($this->getSubject()->getProvider()->getId())){
            $query->andWhere('a.provider = '.$this->getSubject()->getProvider()->getId());
        }
    
        $formMapper->tab('Factura');
        $formMapper->with('');
        $formMapper->add('provider', null, ['label' => 'Proveedor']);
        $formMapper->add('number', null, ['label' => 'Nº Factura']);
        $formMapper->add('date', 'sonata_type_date_picker', ['label' => 'Fecha']);
        $formMapper->add('base', MoneyType::class, ['label' => 'Base (Si no se indica es calculado con los importes de los albaranes)', 'currency' => 'EUR', 'required' => false]);
        $formMapper->add('tax', MoneyType::class, ['label' => 'IVA (Si no se indica es calculado con los importes de los albaranes)', 'currency' => 'EUR', 'required' => false]);
        $formMapper->add('total', MoneyType::class, ['label' => 'Total (Si no se indica es calculado con los importes de los albaranes)', 'currency' => 'EUR', 'required' => false]);
        $formMapper->add('paid', null, ['label' => 'Pagada']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Albaranes');
        $formMapper->with('');
        $formMapper->add('deliveryNotes', 'sonata_type_model', [
                'label' => 'Albaranes', 
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
                'query' => $query,
            ]
        );
        $formMapper->end();
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('provider', null, ['label' => 'Proveedor']);
        $datagridMapper->add('number', null, ['label' => 'Nº Factura']);
        $datagridMapper->add('date', 'doctrine_orm_date_range', 
                ['label' => 'Fecha', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('paid', null, ['label' => 'Pagada']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('provider', null, ['label' => 'Proveedor']);
        $listMapper->add('number', null, ['label' => 'Nº Factura']);
        $listMapper->add('date', null, ['label' => 'Fecha']);
        $listMapper->add('total', MoneyType::class, ['label' => 'Total', 'currency' => 'EUR']);  
        $listMapper->add('paid', null, ['label' => 'Pagada', 'editable' => true]); 
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('provider', null, ['label' => 'Proveedor']);
        $showMapper->add('number', null, ['label' => 'Nº Factura']);
        $showMapper->add('date', null, ['label' => 'Fecha']);
        $showMapper->add('base', 'currency', ['label' => 'Base', 'currency' => 'EUR']);
        $showMapper->add('tax', 'currency', ['label' => 'IVA', 'currency' => 'EUR']);
        $showMapper->add('total', 'currency', ['label' => 'Total', 'currency' => 'EUR']);
        $showMapper->add('paid', null, ['label' => 'Pagada']);
        $showMapper->add('deliveryNotes', null, ['label' => 'Albaranes']);
    }
}

