<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use AppBundle\Form\Type\CustomerType;
use AppBundle\Form\Type\AddrType;
use AppBundle\Form\Type\VehicleType;

class InvoiceReceiptAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    public function getNewInstance()
    {

    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->tab('Recibo');
        $formMapper->with('');
        $formMapper->add('invoice', 'sonata_type_model_autocomplete', 
                    [
                        'label' => 'Factura',
                        'required' => false,
                        'property' => ['number'],
                        'placeholder' => 'Por favor, introduzca 3 caracteres. Puede buscar por Numero de Factura',
                    ]);
        $formMapper->add('description', null, ['label' => 'Descripción']);
        $formMapper->add('amount', MoneyType::class, ['label' => 'Cantidad', 'currency' => 'EUR']);
        $formMapper->add('paid', MoneyType::class, ['label' => 'Cantidad Pagada', 'currency' => 'EUR']);
        if (!empty($this->getSubject()) && !$this->getSubject()->isPaid()){
            $formMapper->add('is_paid', 'checkbox', ['label' => 'Pagada', 'required' => false]);
        }else{
            $formMapper->add('is_paid', 'checkbox', ['label' => 'Pagada', 'required' => false, 'disabled' => true]);
        }
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Cliente');
        $formMapper->with('');
        $formMapper->add('customer', CustomerType::class, ['label' => 'Cliente', 'required' => false]);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Dirección');
        $formMapper->with('');
        $formMapper->add('address',  AddrType::class, ['label' => 'Dirección', 'required' => false]);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Vehículo');
        $formMapper->with('');
        $formMapper->add('vehicle',  VehicleType::class, ['label' => 'Vehículo', 'required' => false]);
        $formMapper->end();
        $formMapper->end();
   
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('invoice.number', null, ['label' => 'Número']);
        $datagridMapper->add('paid', null, ['label' => 'Pagada']);
//        $datagridMapper->add('customer.identifier', null, [
//                'label' => 'Nif',
//                'field_name' => 'customer.identifier'
//            ]);
//        $datagridMapper->add('customer.fullname', null, [
//                'label' => 'Nombre',
//                'field_name' => 'customer.fullname'
//            ]);
//        $datagridMapper->add('vehicle.registration', null, [
//                'label' => 'Matricula',
//                'field_name' => 'vehicle.registration'
//            ]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('invoice.number', null, ['label' => 'Número']);
        $listMapper->add('customer.identifier', null, ['label' => 'Nif']);
        $listMapper->add('customer.fullname', null, ['label' => 'Cliente']);
        $listMapper->add('vehícle.registration', null, ['label' => 'Matrícula']);
        $listMapper->add('invoice.oe.km', null, ['label' => 'Km']);
        $listMapper->add('amount', null, ['label' => 'Cantidad', 'currency' => 'EUR']);
        $listMapper->add('isPaid', 'boolean', ['label' => 'Pagada', 'editable' => true]);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [],
                'listActions' => [
                    'template' => 'AppBundle:Admin:listActionInvoiceReceipt.html.twig'
                ],
                ]]);
    }
    
   protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('invoice.number', null, ['label' => 'Número']);
        $showMapper->add('description', null, ['label' => 'Decripción']);
        $showMapper->add('amount', MoneyType::class, ['label' => 'Cantidad', 'currency' => 'EUR']);
        $showMapper->add('paid', 'currency', ['label' => 'Cantidad Pagada', 'currency' => 'EUR']);
        $showMapper->add('is_paid', 'boolean', ['label' => 'Pagada']);
        $showMapper->add('customer', null, ['label' => 'Cliente']);
        $showMapper->add('address', null, ['label' => 'Direccion']);
        $showMapper->add('vehicle', null, ['label' => 'Vehiculo']);
        $showMapper->add('actions' , null, [
                    'template' => 'AppBundle:Admin:showActionInvoiceReceipt.html.twig'
                ]
        );
    }

    public function createQuery($context = 'list')
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();

        $query = parent::createQuery($context);
        if ($company && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $query->innerJoin($query->getRootAliases()[0] . '.invoice', 'i');
            $query->innerJoin('i.oe', 'oe');
            $query->andWhere('oe.company = :company');
            $query->setParameter('company', $company);
        }
        return $query;
    }
}

