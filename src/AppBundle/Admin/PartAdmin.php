<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class PartAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];

    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
        
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();        
        if ($company){
            $entity->setTax($company->getDefaultTax());
        }
        
        $entity->setDiscount(0);
 
        return $entity;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('provider', 'sonata_type_model', ['label' => 'Proveedor', 'required' => false]);
        $formMapper->add('ref', null, ['label' => 'Referencia']);
        $formMapper->add('description', null, ['label' => 'Descripción']);
        $formMapper->add('trademark', null, ['label' => 'Marca']);
        $formMapper->add('vehicle', null, ['label' => 'Vehiculo (Marca Modelo)']);
        $formMapper->add('discount', 'percent', ['label' => 'Descuento', 'scale' => 2]);
        $formMapper->add('discountNext', 'percent', ['label' => 'Descuento Sucesivo', 'scale' => 2, 'required' => false]);
        $formMapper->add('tax', 'percent', ['label' => 'IVA']);
        $formMapper->add('price', MoneyType::class, ['label' => 'Precio', 'currency' => 'EUR']);
        $formMapper->add('stock', null, ['label' => 'Stock (modificar solo si hay entrada en almacén sin albarán)']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('ref', null, ['label' => 'Referencia']);
        $datagridMapper->add('description', null, ['label' => 'Descripcion']);
        $datagridMapper->add('trademark', null, ['label' => 'Marca']);
        $datagridMapper->add('vehicle', null, ['label' => 'Vehículo']);
        $datagridMapper->add('provider', null, ['label' => 'Proveedor']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('ref', null, ['label' => 'Referencia']);
        $listMapper->add('description', null, ['label' => 'Descripción']);
        $listMapper->add('price','currency', ['label' => 'Precio', 'currency' => 'EUR']);
        $listMapper->add('stock',null, ['label' => 'Stock']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('provider', null, ['label' => 'Proveedor']);
        $showMapper->add('ref', null, ['label' => 'Referencia']);
        $showMapper->add('description', null, ['label' => 'Descripcion']);
        $showMapper->add('trademark', null, ['label' => 'Marca']);
        $showMapper->add('vehicle', null, ['label' => 'Vehículo']);
        $showMapper->add('discount', 'percent', ['label' => 'Descuento']);
        $showMapper->add('tax', 'percent', ['label' => 'IVA']);
        $showMapper->add('price', 'currency', ['label' => 'Precio', 'currency' => 'EUR']);
        $showMapper->add('stock', null, ['label' => 'Stock']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
    }
}

