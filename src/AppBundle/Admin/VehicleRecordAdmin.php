<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class VehicleRecordAdmin extends Admin
{
    protected $baseRouteName = 'admin_app_vehiclerecord';

    protected $baseRoutePattern = 'app/vehiclerecord';
    
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('registration', 'doctrine_orm_callback', [
                'label' => 'Matricula',
                'field_name' => 'vehicle_data.registration',
                'callback' => function($queryBuilder, $alias, $field, $value) {
                    if (!$value['value']) {
                        return;
                    }

                    $queryBuilder->innerJoin('AppBundle:Invoice', 'i', 'with', "i.id = $alias.invoice");
                    $queryBuilder->andWhere("i.vehicle_data.registration like :registration");
                    $queryBuilder->setParameter('registration', "%{$value['value']}%");

                    return true;
                },
            ]);
        $datagridMapper->add('invoice.number', null, ['label' => 'Nº Factura']);
        $datagridMapper->add('invoice.date', 'doctrine_orm_date_range', 
                ['label' => 'Fecha', 'field_type'=>'sonata_type_datetime_range_picker']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('invoice.number', null, ['label' => 'Nº Factura']);
        $listMapper->add('invoice.date', 'date', ['label' => 'Fecha']);
        $listMapper->add('invoice.km', null, ['label' => 'Km']);
        $listMapper->add('item.ref', null, ['label' => 'Ref.']);
        $listMapper->add('amount', null, ['label' => 'C/H']);
        $listMapper->add('item.provider', null, ['label' => 'Proveedor']);
        $listMapper->add('description', null, ['label' => 'Descripción']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('invoice.number', null, ['label' => 'Nº Factura']);
        $showMapper->add('invoice.date', 'date', ['label' => 'Fecha']);
        $showMapper->add('item.provider', null, ['label' => 'Proveedor']);
        $showMapper->add('item.ref', null, ['label' => 'Referencia']);
        $showMapper->add('description', null, ['label' => 'Descripción']);
        $showMapper->add('amount', null, ['label' => 'Cant./Horas']);
        $showMapper->add('price', MoneyType::class, ['label' => 'Precio Unit.', 'currency' => 'EUR']);
        $showMapper->add('discount', 'percent', ['label' => 'Descuento']);
        $showMapper->add('tax', 'percent', ['label' => 'IVA']);
        $showMapper->add('base', 'currency', ['label' => 'Total', 'currency' => 'EUR']);
        $showMapper->add('total', 'currency', ['label' => 'Total (IVA incluido)', 'currency' => 'EUR']);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // Se pone "create" para que se pueda añadir desde el admin de factura
        // @todo: buscar una forma para que solo se pueda añadir desde factura (embebidos) 
        $collection->clearExcept(['list', 'show', 'export']);      
    }
}

