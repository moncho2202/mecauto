<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class VehicleAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
        $customer = false;
        $id = $this->getRequest()->query->get('cod', false);
        if ($id){
            $customer = $this->getModelManager()->findOneBy('AppBundle:Customer', ['id' => $id]);
        }
        if ($customer){
            $entity->setCustomer($customer);
        }
 
        return $entity;
    } 
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();
        
        $formMapper->tab('Vehículo');
        $formMapper->with('');
        if ($this->getRoot()->getClass() == 'AppBundle\Entity\Vehicle') {
            $formMapper->add('customer', 'sonata_type_model_autocomplete', ['property' => ['id', 'identifier', 'fullname']]);
        }
        $formMapper->add('registration', null, ['label' => 'Matrícula', 'help' => 'Para forzar la matrícula introducir delante %F%']);
        $formMapper->add('trademark', null, ['label' => 'Marca']);
        $formMapper->add('model', null, ['label' => 'Modelo']);
        $formMapper->add('engine', null, ['label' => 'Motor']);
        $formMapper->add('colour', null, ['label' => 'Color']);
        $formMapper->add('colour_code', null, ['label' => 'Código Color']);
        $formMapper->add('vin', null, ['label' => 'Bastidor']);
        $formMapper->add('date_making', 'sonata_type_date_picker', ['label' => 'Fecha de Fabricación', 'required' => false]);
        $formMapper->add('note', null, ['label' => 'Observaciones']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Ficheros');
        $formMapper->with('');
        $formMapper->add('files', 'sonata_type_collection', 
                ['label' => ' ', 'by_reference' => false, 'required' => false], 
                ['edit' => 'inline']);
        $formMapper->end();
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('registration', null, ['label' => 'Matricula']);
        $datagridMapper->add('trademark', null, ['label' => 'Marca']);
        $datagridMapper->add('model', null, ['label' => 'Modelo']);
        $datagridMapper->add('engine', null, ['label' => 'Motor']);
        $datagridMapper->add('colour', null, ['label' => 'Color']);
        $datagridMapper->add('colour_code', null, ['label' => 'Código Color']);
        $datagridMapper->add('vin', null, ['label' => 'Bastidor']);
        $datagridMapper->add('created_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Creado', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('updated_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Modificado', 'field_type'=>'sonata_type_datetime_range_picker']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('registration', null, ['label' => 'Matricula']);
        $listMapper->add('trademarkModel', null, ['label' => 'Marca y Modelo']);
        $listMapper->add('customer', null, ['label' => 'Cliente']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [],
                'listActions' => [
                    'template' => 'AppBundle:Admin:listActionVehicle.html.twig'
                ], 
                ]]);
    }
    
   protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('registration', null, ['label' => 'Matricula']);
        $showMapper->add('customer', null, ['label' => 'Cliente']);
        $showMapper->add('trademark', null, ['label' => 'Marca']);
        $showMapper->add('model', null, ['label' => 'Modelo']);
        $showMapper->add('engine', null, ['label' => 'Motor']);
        $showMapper->add('colour', null, ['label' => 'Color']);
        $showMapper->add('colour_code', null, ['label' => 'Código Color']);
        $showMapper->add('vin', null, ['label' => 'Bastidor']);
        $showMapper->add('date_making', null, ['label' => 'Fecha de Fabricación']);
        $showMapper->add('note', null, ['label' => 'Observaciones']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
        $showMapper->add('actions' , null, [
                    'template' => 'AppBundle:Admin:showActionVehicle.html.twig'
                ]
        );
    }
}

