<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class LaborAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];

    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
        
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();        
        if ($company){
            $entity->setTax($company->getDefaultTax());
        }
        
        $entity->setDiscount(0);
 
        return $entity;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $query = $this->modelManager->getEntityManager('AppBundle\Entity\LaborType')
                ->createQueryBuilder()
                ->from('AppBundle:LaborType', 't')
                ->select('t')
                ->where('t.active = 1');

        $formMapper->add('ref', null, ['label' => 'Referencia']);
        $formMapper->add('description', null, ['label' => 'Descripción']);
        $formMapper->add('type', 'sonata_type_model', ['label' => 'Tipo', 'query' => $query]);
        $formMapper->add('price', MoneyType::class, ['label' => 'Precio', 'currency' => 'EUR']);
        $formMapper->add('hour', 'number', ['label' => 'Horas', 'scale' => 2]);
        $formMapper->add('discount', 'percent', ['label' => 'Descuento']);
        $formMapper->add('tax', 'percent', ['label' => 'IVA']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('ref', null, ['label' => 'Referencia']);
        $datagridMapper->add('description', null, ['label' => 'Descripcion']);
        $datagridMapper->add('type', null, ['label' => 'Tipo']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('ref', null, ['label' => 'Referencia']);
        $listMapper->add('description', null, ['label' => 'Descripción']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('ref', null, ['label' => 'Referencia']);
        $showMapper->add('description', null, ['label' => 'Descripcion']);
        $showMapper->add('type', null, ['label' => 'Tipo']);
        $showMapper->add('price', MoneyType::class, ['label' => 'Precio', 'currency' => 'EUR']);
        $showMapper->add('hour', 'number', ['label' => 'Horas', 'scale' => 2]);
        $showMapper->add('discount', 'percent', ['label' => 'Descuento']);
        $showMapper->add('tax', 'percent', ['label' => 'IVA']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
    }
}

