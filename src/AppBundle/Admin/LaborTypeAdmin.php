<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class LaborTypeAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', null, ['label' => 'Nombre']);
        $formMapper->add('description', null, ['label' => 'Descripción']);
        $formMapper->add('active', null, ['label' => 'Activo']);
        $formMapper->add('excess', null, ['label' => 'Franquicia']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('name', null, ['label' => 'Nombre']);
        $listMapper->add('description', null, ['label' => 'Descripción']);
        $listMapper->add('active', null, ['label' => 'Activo', 'editable' => true]);
        $listMapper->add('excess', null, ['label' => 'Franquicia', 'editable' => true]);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('name', null, ['label' => 'Name']);
        $showMapper->add('description', null, ['label' => 'Descripcion']);
        $showMapper->add('active', null, ['label' => 'Activo']);
        $showMapper->add('excess', null, ['label' => 'Franquicia']);
    }
}

