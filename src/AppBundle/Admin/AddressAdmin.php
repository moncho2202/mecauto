<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;

use AppBundle\Form\Type\AddrType;

class AddressAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('addr', AddrType::class, ['label' => 'Dirección']);
    }
}

