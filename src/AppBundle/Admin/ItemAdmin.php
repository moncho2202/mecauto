<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ItemAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );

    protected function configureFormFields(FormMapper $formMapper)
    {

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('ref', null, ['label' => 'Referencia']);
        $datagridMapper->add('description', null, ['label' => 'Descripcion']);
        $datagridMapper->add('provider', null, ['label' => 'Proveedor']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {

    }
}

