<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class DeliveryNotePartAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );

    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
        
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();        
        if ($company){
            $entity->setTax($company->getDefaultTax());
        }
        
        $entity->setDiscount(0);
 
        return $entity;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('part', 'sonata_type_model_autocomplete', 
                    [
                        'label' => 'Artículo',
                        'property' => ['id', 'ref', 'description'],
                        'placeholder' => 'Por favor, introduzca 3 caracteres. Puede buscar por Referencia, Descripcion y Id',
                        'template' => 'AppBundle:Admin:sonata_type_model_autocomplete_add.html.twig'
                    ]
            );
        $formMapper->add('amount', null, ['label' => 'Cantidad']);
        $formMapper->add('discount', 'percent', ['label' => 'Descuento', 'scale' => 2]);
        $formMapper->add('discountNext', 'percent', ['label' => 'Descuento Sucesivo', 'scale' => 2, 'required' => false]);
        $formMapper->add('priceNeto', MoneyType::class, ['label' => 'Precio Neto', 'currency' => 'EUR', 'scale' => 2, 'required' => false]);
        $formMapper->add('tax', 'percent', ['label' => 'IVA']);
        $formMapper->add('price', MoneyType::class, ['label' => 'Precio Unit.', 'currency' => 'EUR']);
        $formMapper->add('base', MoneyType::class, ['label' => 'Total', 'required' => false, 'disabled' => true, 'currency' => 'EUR']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    protected function configureListFields(ListMapper $listMapper)
    {

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {

    }
}

