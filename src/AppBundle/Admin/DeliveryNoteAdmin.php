<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class DeliveryNoteAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('provider', null, ['label' => 'Proveedor']);
        $formMapper->add('number', null, ['label' => 'Número']);
        $formMapper->add('date', 'sonata_type_date_picker', ['label' => 'Fecha']);
        $formMapper->add('parts', 'sonata_type_collection',
                ['label' => 'Artículos', 'by_reference' => false], 
                ['edit' => 'inline', 'inline' => 'table']);
        $formMapper->add('base', MoneyType::class, ['label' => 'Total', 'currency' => 'EUR', 'disabled' => true, 'required' => false]);
        $formMapper->add('tax', MoneyType::class, ['label' => 'IVA', 'currency' => 'EUR', 'disabled' => true, 'required' => false]);
        $formMapper->add('total', MoneyType::class, ['label' => 'Total (IVA incluido)', 'currency' => 'EUR', 'disabled' => true, 'required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('number', null, ['label' => 'Número']);
        $datagridMapper->add('date', 'doctrine_orm_date_range', 
                ['label' => 'Fecha', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('parts.part.ref', null, ['label' => 'Referencia']);
        $datagridMapper->add('parts.part.trademark', null, ['label' => 'Marca']);
        $datagridMapper->add('parts.part.vehicle', null, ['label' => 'Vehículo']);
        $datagridMapper->add('invoice.number', null, ['label' => 'Factura']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('number', null, ['label' => 'Número']);
        $listMapper->add('provider', null, ['label' => 'Proveedor']);
        $listMapper->add('date', 'date', ['label' => 'Fecha']);
        $listMapper->add('total', 'currency', ['label' => 'Total', 'currency' => 'EUR']);
        $listMapper->add('invoice.number', 'boolean', ['label' => 'Factura']);
        $listMapper->add('paid', 'boolean', ['label' => 'Pagado']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('provider', null, ['label' => 'Proveedor']);
        $showMapper->add('number', null, ['label' => 'Número']);
        $showMapper->add('date', 'date', ['label' => 'Fecha']);
        $showMapper->add('parts', null, ['label' => 'Artículos']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
        $showMapper->add('total', 'currency', ['label' => 'Total', 'currency' => 'EUR']);
        $showMapper->add('invoice', null, ['label' => 'Factura']);
        $showMapper->add('paid', 'boolean', ['label' => 'Pagado']);
    }
}

