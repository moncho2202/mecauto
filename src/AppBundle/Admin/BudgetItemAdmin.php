<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class BudgetItemAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );

    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
        
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();        
        if ($company){
            $entity->setTax($company->getDefaultTax());
        }
 
        return $entity;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('item', 'sonata_type_model_autocomplete', 
                    [
                        'label' => 'Línea (Artículo ó MO)',
                        'property' => ['id', 'ref', 'description'],
                        'placeholder' => 'Por favor, introduzca 3 caracteres. Puede buscar por Referencia, Descripcion y Id',
                    ],
                    []
            );
        $formMapper->add('description', null, ['label' => 'Descripción', 'required' => false]);
        $formMapper->add('amount', null, ['label' => 'Cant./Horas', 'required' => false]);
        $formMapper->add('discount', 'percent', ['label' => 'Descuento', 'required' => false]);
        $formMapper->add('tax', 'percent', ['label' => 'IVA', 'required' => false]);
        $formMapper->add('price', null, ['label' => 'Precio Unit.', 'required' => false]);
        $formMapper->add('base', MoneyType::class, ['label' => 'Total', 'required' => false, 'disabled' => true, 'currency' => 'EUR']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    protected function configureListFields(ListMapper $listMapper)
    {

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {

    }
}

