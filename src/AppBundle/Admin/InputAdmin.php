<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class InputAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('part', null, ['label' => 'Articulo']);
        $datagridMapper->add('created_at', 'doctrine_orm_datetime_range',
                ['label' => 'Creado', 'field_type'=>'sonata_type_datetime_range_picker']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('part', null, ['label' => 'Artículo']);
        $listMapper->add('amount', null, ['label' => 'Cantidad']);
        $listMapper->add('deliveryNotePart.deliveryNote', null, ['label' => 'Albarán']);
        $listMapper->add('created_at', null, ['label' => 'Creado']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('part', null, ['label' => 'Artículo']);
        $showMapper->add('amount', null, ['label' => 'Cantidad']);
        $showMapper->add('deliveryNotePart.deliveryNote', null, ['label' => 'Albarán']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'show', 'export'));
    }
}

