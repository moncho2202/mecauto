<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderRepairAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
        $oe = false;
        $budget = false;
        $oe_id = $this->getRequest()->query->get('cod', false);
        $budget_id = $this->getRequest()->query->get('cod_b', false);
        if ($budget_id){
            $budget = $this->getModelManager()->findOneBy('AppBundle:Budget', ['id' => $budget_id]);
        }
        if ($budget){
            if ($oe_id && $oe_id == $budget->getOe()->getId()){
                $entity->copyBudget($budget);
            } else {
                // Error: La OE no es la misma que la del Budget
                // @TODO Mostrar aviso (mensaje flash) y escribir en log
            }
        }elseif ($oe_id){
            if ($oe = $this->getModelManager()->findOneBy('AppBundle:OrderEntry', ['id' => $oe_id])){
                $entity->setOe($oe);
                $entity->setDescription($oe->getDescription());
            }
        }
 
        return $entity;
    }
    
    public function prePersist($object)
    {
        parent::prePersist($object);
        
        if (!$object->getDescription() && $object->getOe()){
            $object->setDescription($object->getOe()->getDescription());
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('oe', 'sonata_type_model_autocomplete', 
                [
                    'label' => 'Orden de Entrada',
                    'property' => ['vehiculo.matricula'],
                    'placeholder' => 'Por favor, introduzca 3 caracteres. Puede buscar por Matricula, Nif, Nombre y Id',
                    'callback' => function ($admin, $property, $value) {
                        $datagrid = $admin->getDatagrid();
                        $queryBuilder = $datagrid->getQuery();
                        $queryBuilder
                            ->innerJoin($queryBuilder->getRootAlias() . '.customer', 'c')
                            ->innerJoin($queryBuilder->getRootAlias() . '.vehicle', 'v')
                            ->orWhere($queryBuilder->getRootAlias() . '.id = :barValue')
                            ->orWhere('v.id = :barValue')
                            ->orWhere('v.registration like :likeValue')
                            ->orWhere('c.id = :barValue')
                            ->orWhere('c.identifier like :likeValue')
                            ->orWhere('c.fullname like :likeValue')
                            ->setParameter('barValue', $value)
                            ->setParameter('likeValue', '%'.$value.'%')
                        ;
                    },
                    'to_string_callback' => function($entity, $property) {
                        return $entity->getId()." - ".$entity->getVehicle()->getCustomerVehicle();
                    },
                ]
        );
        $formMapper->add('date', 'sonata_type_date_picker', ['label' => 'Fecha']);
        $formMapper->add('description', null, ['label' => 'Descrición']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('number', null, ['label'=>'Número']);
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        if (!$company){
            $datagridMapper->add('oe.company', null, ['label' => 'Empresa']);
        }
        $datagridMapper->add('oe.identifier', 'doctrine_orm_string', [
                'label' => 'Nif',
                'field_name' => 'customer_data.identifier'
            ]);
        $datagridMapper->add('oe.fullname', 'doctrine_orm_string', [
                'label' => 'Nombre',
                'field_name' => 'customer_data.fullname'
            ]);
        $datagridMapper->add('oe.registration', 'doctrine_orm_string', [
                'label' => 'Matricula',
                'field_name' => 'vehicle_data.registration'
            ]);
        $datagridMapper->add('date', 'doctrine_orm_date_range', 
                ['label' => 'Fecha', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('created_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Creado', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('updated_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Modificado', 'field_type'=>'sonata_type_datetime_range_picker']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('number', null, ['label' => 'Número']);
        
          //  Comentamos la compañia, deberia valer con el numero para distinguir la empresa
          //  @todo: eliminar si no solicitan volver a poner
//        $company = $this->getConfigurationPool()->getContainer()
//                ->get('security.token_storage')->getToken()
//                ->getUser()->getCompany();
//        if (!$company){
//            $listMapper->add('oe.company', null, ['label' => 'Empresa']);
//        }
        
        $listMapper->add('oe.identifier', null, ['label' => 'Nif']);
        $listMapper->add('oe.fullname', null, ['label' => 'Nombre']);
        $listMapper->add('oe.registration', null, ['label' => 'Matricula']);
        $listMapper->add('oe.km', null, ['label' => 'Km']);
        $listMapper->add('date', 'date', ['label' => 'Fecha']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [],
                'listActions' => [
                    'template' => 'AppBundle:Admin:listActionOrderRepair.html.twig'
                ],
                ]]);
    }
    
   protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('number', null, ['label' => 'Número']);
        $showMapper->add('oe.company', null, ['label' => 'Empresa']);
        $showMapper->add('oe.customer', null, ['label' => 'Id Cliente', 'associated_property' => 'id']);
        $showMapper->add('oe.customer_data', null, ['label' => 'Cliente']);
        $showMapper->add('oe.address_data', null, ['label' => 'Direccion']);
        $showMapper->add('oe.vehicle', null, ['label' => 'Id Vehículo', 'associated_property' => 'id']);
        $showMapper->add('oe.vehicle_data', null, ['label' => 'Vehiculo']);
        $showMapper->add('oe.km', null, ['label' => 'Kms']);
        $showMapper->add('oe.deposit', null, ['label' => 'Deposito']);
        $showMapper->add('oe.date_in', 'date', ['label' => 'Fecha de entrada']);
        $showMapper->add('oe.date_out', 'date', ['label' => 'Fecha de salida']);
        $showMapper->add('date', 'date', ['label' => 'Fecha']);
        $showMapper->add('description', null, ['label' => 'Descripción']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
        $showMapper->add('actions' , null, [
                    'template' => 'AppBundle:Admin:showActionOrderRepair.html.twig'
                ]
        );
    }

    public function createQuery($context = 'list')
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();

        $query = parent::createQuery($context);
        if ($company && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $query->innerJoin($query->getRootAliases()[0] . '.oe', 'oe');
            $query->andWhere('oe.company = :company');
            $query->setParameter('company', $company);
        }
        return $query;
    }
}

