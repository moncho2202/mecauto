<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use AppBundle\Entity\Customer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CustomerAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->tab('Cliente')
            ->with('')
                ->add('identifier', null, ['label' => 'Nif', 'help' => 'Para forzar el Nif introducir delante %F%'])
                ->add('name', null, ['label' => 'Nombre'])
                ->add('surname1', null, ['label' => 'Primer Apellido'])
                ->add('surname2', null, ['label' => 'Segundo Apellido'])
                ->add('email', null, ['label' => 'Email'])
                ->add('phone', null, ['label' => 'Tlf'])
                ->add('movil', null, ['label' => 'Movil'])
                ->add('note', null, ['label' => 'Observaciones'])
            ->end()
        ->end();
        $formMapper->tab('Direcciones')
            ->with('')
                ->add('address', 'sonata_type_collection', 
                        ['label' => 'Direcciones', 'by_reference' => false], 
                        ['edit' => 'inline', 'inline' => 'table', 'by_reference' => false])
            ->end()
        ->end();
        $formMapper->tab('Vehiculos')
            ->with('')
                ->add('vehicles', 'sonata_type_collection', 
                        ['label' => 'Vehiculos', 'by_reference' => false], 
                        ['edit' => 'inline'])
            ->end()
        ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {        
        $datagridMapper->add('id');
        $datagridMapper->add('identifier', null, ['label' => 'Nif']);
        $datagridMapper->add('fullname', null, ['label' => 'Nombre']);
        $datagridMapper->add('email', null, ['label' => 'Email']);
        $datagridMapper->add('phone', null, ['label' => 'Tlf']);
        $datagridMapper->add('movil', null, ['label' => 'Movil']);
        $datagridMapper->add('vehicles.registration', null, ['label' => 'Matricula']);
        $datagridMapper->add('type', 'doctrine_orm_string', ['label' => 'Tipo'], 'choice', ['choices' => Customer::getTypes()]);
        $datagridMapper->add('created_at', 'doctrine_orm_datetime_range',
                ['label' => 'Creado', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('updated_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Modificado', 'field_type'=>'sonata_type_datetime_range_picker']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('identifier', null, ['label' => 'Nif']);
        $listMapper->add('fullname', null, ['label' => 'Nombre']);
        $listMapper->add('phone', null, ['label' => 'Tlf']);
        $listMapper->add('movil', null, ['label' => 'Movil']);
        $listMapper->add('email', null, ['label' => 'Email']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [],
                'listActions' => [
                    'template' => 'AppBundle:Admin:listActionCustomer.html.twig'
                ],
                ]]);
    }
    
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('typeDesc', null, ['label' => 'Tipo']);
        $showMapper->add('identifier', null, ['label' => 'Nif']);
        $showMapper->add('name', null, ['label' => 'Nombre']);
        $showMapper->add('surname1', null, ['label' => 'Primer Apellido']);
        $showMapper->add('surname2', null, ['label' => 'Segundo Apellido']);
        $showMapper->add('fullname', null, ['label' => 'Nombre Completo']);
        $showMapper->add('email', null, ['label' => 'Email']);
        $showMapper->add('phone', null, ['label' => 'Tlf']);
        $showMapper->add('movil', null, ['label' => 'Movil']);
        $showMapper->add('note', null, ['label' => 'Observaciones']);
        $showMapper->add('address', null, ['label' => 'Direcciones']);
        $showMapper->add('vehicles', null, ['label' => 'Vehiculos']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
        $showMapper->add('actions' , null, [
                    'template' => 'AppBundle:Admin:showActionCustomer.html.twig'
                ]
        );
    }
}

