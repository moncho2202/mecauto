<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;

class NumbersAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('numberOe', null, ['label' => 'Número OE']);
        $formMapper->add('numberOr', null, ['label' => 'Número OR']);
        $formMapper->add('numberBudget', null, ['label' => 'Número Presupuesto']);
        $formMapper->add('numberInvoice', null, ['label' => 'Número Factura']);
        $formMapper->add('numberInvoiceRefund', null, ['label' => 'Número Abono']);
    }
}

