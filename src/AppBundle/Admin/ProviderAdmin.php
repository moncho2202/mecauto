<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use AppBundle\Form\Type\AddrType;

class ProviderAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->tab('Proveedor');
        $formMapper->with('');
        $formMapper->add('identifier', null, ['label' => 'Nif']);
        $formMapper->add('alias', null, ['label' => 'Alias']);
        $formMapper->add('name', null, ['label' => 'Nombre']);
        $formMapper->add('email', null, ['label' => 'Email']);
        $formMapper->add('phone', null, ['label' => 'Teléfono']);
        $formMapper->add('movil', null, ['label' => 'Movil']);
        $formMapper->add('note', null, ['label' => 'Observaciones']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Dirección');
        $formMapper->with('');
        $formMapper->add('addr', AddrType::class, ['label' => 'Dirección']);
        $formMapper->end();
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('identifier', null, ['label' => 'Nif']);
        $datagridMapper->add('alias', null, ['label' => 'Alias']);
        $datagridMapper->add('name', null, ['label' => 'Nombre']);
        $datagridMapper->add('email', null, ['label' => 'Email']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('alias', null, ['label' => 'Alias']);
        $listMapper->add('identifier', null, ['label' => 'Nif']);
        $listMapper->add('name', null, ['label' => 'Nombre']);   
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('identifier', null, ['label' => 'Nif']);
        $showMapper->add('alias', null, ['label' => 'Alias']);
        $showMapper->add('name', null, ['label' => 'Nombre']);
        $showMapper->add('street', null, ['label' => 'Dirección']);
        $showMapper->add('code', null, ['label' => 'Código Postal']);
        $showMapper->add('city', null, ['label' => 'Ciudad']);
        $showMapper->add('state', null, ['label' => 'Provincia']);
        $showMapper->add('email', null, ['label' => 'Email']);
        $showMapper->add('phone', null, ['label' => 'Teléfono']);
        $showMapper->add('movil', null, ['label' => 'Movil']);
        $showMapper->add('note', null, ['label' => 'Observaciones']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
    }
}

