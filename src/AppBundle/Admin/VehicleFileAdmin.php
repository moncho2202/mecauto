<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Symfony\Component\Form\Extension\Core\Type\FileType;

class VehicleFileAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('file', $this->getRouterIdParameter().'/file');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('description', null, ['label' => 'Descripción']);
        if ($this->getSubject() && $this->id($this->getSubject())){
            //EDIT
            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = ['label' => 'Fichero    ', 'required' => false];

            $fullPath = "data:".$this->getSubject()->getMime().";base64,".$this->getSubject()->getFileAsBase64();
            $fileFieldOptions['help'] = '<div><a href="'.$this->generateUrl('file', ['id' => $this->getSubject()->getId()]).'" target="_blank"><i class="fa fa-search-plus">Mostrar</i></a></div>';
            // add a 'help' option containing the preview's img tag
            if (substr($this->getSubject()->getMime(), 0, 5) == 'image'){
                $fileFieldOptions['help'] .= '<div><img src="'.$fullPath.'" class="admin-preview" style="max-width:600px" /></div>';
            }else{
                $fileFieldOptions['help'] .= '<div><embed src="'.$fullPath.'" aplication="'.$this->getSubject()->getMime().'" /></div>';
            }
            $fileFieldOptions['data_class'] = null;
            $fileFieldOptions['disabled'] = true;
            $fileFieldOptions['attr'] = ['style' => 'display:none'];
            
            $formMapper->add('filename', FileType::class, $fileFieldOptions);

        }else{
            //CREATE
            $formMapper->add('filename', FileType::class, ['label' => 'Fichero']);
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    protected function configureListFields(ListMapper $listMapper)
    {

    }
    
   protected function configureShowFields(ShowMapper $showMapper)
    {

    }
}

