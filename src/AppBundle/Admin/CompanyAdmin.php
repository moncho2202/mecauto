<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;

use AppBundle\Form\Type\AddrType;

class CompanyAdmin extends Admin
{
    protected $formOptions = array(
        'trim' => true,
    );
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->tab('Empresa');
        $formMapper->with('');
        $formMapper->add('identifier', null, ['label' => 'Nif']);
        $formMapper->add('alias', null, ['label' => 'Alias']);
        $formMapper->add('name', null, ['label' => 'Nombre']);
        $formMapper->add('email', null, ['label' => 'Email']);
        $formMapper->add('phone', null, ['label' => 'Teléfono']);
        $formMapper->add('movil', null, ['label' => 'Movil']);
        $formMapper->add('registry', null, ['label' => 'Registro']);
        $formMapper->add('registered', null, ['label' => 'Inscrita']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Por defecto');
        $formMapper->with('');
        $formMapper->add('default_tax', 'percent', ['label' => 'IVA por defecto']);
        $formMapper->add('default_price_by_hour', MoneyType::class, ['label' => 'Precio Hora por defecto', 'currency' => 'EUR']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Dirección');
        $formMapper->with('');
        $formMapper->add('addr', AddrType::class, ['label' => 'Dirección']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Prefijos');
        $formMapper->with('');
        $formMapper->add('prefix_oe', null, ['label' => 'Prefijo de OE']);
        $formMapper->add('prefix_or', null, ['label' => 'Prefijo de OR']);
        $formMapper->add('prefix_budget', null, ['label' => 'Prefijo de Presupuesto']);
        $formMapper->add('prefix_invoice', null, ['label' => 'Prefijo de Factura']);
        $formMapper->add('prefix_invoice_refund', null, ['label' => 'Prefijo de Abono']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Numeración');
        $formMapper->with('');
        $formMapper->add('numbers', 'sonata_type_admin', ['label' => 'Numeración']);
        $formMapper->end();
        $formMapper->end();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id', null, ['route' => ['name' => 'show']]);
        $listMapper->addIdentifier('alias', null, ['label' => 'Alias']);
        $listMapper->add('identifier', null, ['label' => 'Nif']);
        $listMapper->add('name', null, ['label' => 'Nombre']);   
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [], 
                ]]);
    }
    
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('identifier', null, ['label' => 'Nif']);
        $showMapper->add('alias', null, ['label' => 'Alias']);
        $showMapper->add('name', null, ['label' => 'Nombre']);
        $showMapper->add('street', null, ['label' => 'Dirección']);
        $showMapper->add('code', null, ['label' => 'Código Postal']);
        $showMapper->add('city', null, ['label' => 'Ciudad']);
        $showMapper->add('state', null, ['label' => 'Provincia']);
        $showMapper->add('email', null, ['label' => 'Email']);
        $showMapper->add('phone', null, ['label' => 'Teléfono']);
        $showMapper->add('movil', null, ['label' => 'Movil']);
        $showMapper->add('registry', null, ['label' => 'Registro']);
        $showMapper->add('registered', null, ['label' => 'Inscrita']);
        $showMapper->add('default_tax', 'percent', ['label' => 'IVA por defecto']);
        $showMapper->add('default_price_by_hour', 'currency', ['label' => 'Precio Hora por defecto', 'currency' => 'EUR']);
        $showMapper->add('prefix_oe', null, ['label' => 'Prefijo de OE']);
        $showMapper->add('prefix_or', null, ['label' => 'Prefijo de OR']);
        $showMapper->add('prefix_budget', null, ['label' => 'Prefijo de Presupuesto']);
        $showMapper->add('prefix_invoice', null, ['label' => 'Prefijo de Factura']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
    }
    
    public function createQuery($context = 'list')
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();

        $query = parent::createQuery($context);
        if ($company && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $query->andWhere($query->getRootAliases()[0] . '.id = :company');
            $query->setParameter('company', $company);
        }
        return $query;
    }
}

