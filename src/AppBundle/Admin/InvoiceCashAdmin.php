<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use AppBundle\Form\Type\CustomerType;
use AppBundle\Form\Type\AddrType;
use AppBundle\Form\Type\VehicleType;

class InvoiceCashAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
//        $oe = false;
//        $budget = false;
//        $oe_id = $this->getRequest()->query->get('cod', false);
//        $budget_id = $this->getRequest()->query->get('cod_b', false);
//        if ($budget_id){
//            $budget = $this->getModelManager()->findOneBy('AppBundle:Budget', ['id' => $budget_id]);
//        }
//        if ($budget){
//            if ($oe_id && $oe_id == $budget->getOe()->getId()){
//                $entity->copyBudget($budget);
//            }else{
//                // Error: La OE no es la misma que la del Budget
//                // @TODO Mostrar aviso (mensaje flash) y escribir en log
//            }
//        }elseif ($oe_id){
//            if ($oe = $this->getModelManager()->findOneBy('AppBundle:OrderEntry', ['id' => $oe_id])){
//                $entity->setOe($oe);
//            }
//        }
 
        return $entity;
    }

    /**
     * @todo: Pasar a un listener
     * @param object $object
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        if ($company){
            $object->setCompany($company);
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()){
            // EDITAR
            $this->addBlockFormEdit($formMapper);
        }else{
            // CREAR
            $this->addBlockFormCreate($formMapper);
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('number', null, ['label' => 'Número']);
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        if (!$company){
            $datagridMapper->add('company', null, ['label' => 'Empresa']);
        }
        $datagridMapper->add('customer_data.identifier', null, [
                'label' => 'Nif',
                'field_name' => 'customer_data.identifier'
            ]);
        $datagridMapper->add('customer_data.fullname', null, [
                'label' => 'Nombre',
                'field_name' => 'customer_data.fullname'
            ]);
        $datagridMapper->add('paid', null, ['label' => 'Pagada']);
        $datagridMapper->add('date', 'doctrine_orm_date_range', 
                ['label' => 'Fecha', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('created_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Creado', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('updated_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Modificado', 'field_type'=>'sonata_type_datetime_range_picker']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('number', null, ['label' => 'Número']);   
        $listMapper->add('customer_data.identifier', null, ['label' => 'Nif']);
        $listMapper->add('customer_data.fullname', null, ['label' => 'Nombre']);
        $listMapper->add('isPaid', 'boolean', ['label' => 'Pagada', 'editable' => true]);
        $listMapper->add('date', 'date', ['label' => 'Fecha']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [],
                'listActions' => [
                    'template' => 'AppBundle:Admin:listActionInvoiceCash.html.twig'
                ],
                ]]);
    }
    
   protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('number', null, ['label' => 'Número']);
        $showMapper->add('company', null, ['label' => 'Empresa']);
        $showMapper->add('customer', null, ['label' => 'Id Cliente', 'associated_property' => 'id']);
        $showMapper->add('customer_data', null, ['label' => 'Cliente']);
        $showMapper->add('address_data', null, ['label' => 'Direccion']);
        $showMapper->add('date', 'date', ['label' => 'Fecha']);
        $showMapper->add('paid', 'currency', ['label' => 'Cantidad Pagada', 'currency' => 'EUR']);
        $showMapper->add('is_paid', 'boolean', ['label' => 'Pagada']);
        $showMapper->add('items', null, ['label' => 'Artículos', 'admin_code' => 'sonata.admin.order.invoice.item']);
        $showMapper->add('refunds', null, ['label' => 'Abonos']);
        $showMapper->add('base', 'currency', ['label' => 'Total', 'currency' => 'EUR']);
        $showMapper->add('tax', 'currency', ['label' => 'IVA', 'currency' => 'EUR']);
        $showMapper->add('total', 'currency', ['label' => 'Total (IVA incluido)', 'currency' => 'EUR']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
        $showMapper->add('actions' , null, [
                    'template' => 'AppBundle:Admin:showActionInvoiceCash.html.twig'
                ]
        );
    }

    public function createQuery($context = 'list')
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();

        $query = parent::createQuery($context);
        if ($company && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $query->andWhere($query->getRootAliases()[0] . '.company = :company');
            $query->setParameter('company', $company);
        }
        return $query;
    }


    /**
     * Formulario de creación
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormCreate(FormMapper $formMapper)
    {
        $this->addBlockFormCompany($formMapper);
        $this->addBlockFormCustomer($formMapper);
        $this->addBlockFormInvoice($formMapper);
    }

    /**
     * Formulario de edición en tabs
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormEdit(FormMapper $formMapper)
    {
        $formMapper->tab('Factura');
        $formMapper->with('');
        $this->addBlockFormCompany($formMapper);
        $this->addBlockFormCustomer($formMapper);
        $this->addBlockFormInvoice($formMapper);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Cliente');
        $formMapper->with('');
        $formMapper->add('customer_data', CustomerType::class, ['label' => 'Cliente']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Dirección');
        $formMapper->with('');
        $formMapper->add('address_data',  AddrType::class, ['label' => 'Dirección']);
        $formMapper->end();
        $formMapper->end();
    }

    /**
     * Bloque para cambiar el cliente en caso de ser distinto de el de la OE 
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormCustomer(FormMapper $formMapper)
    {
        $formMapper->add('customer', 'sonata_type_model_autocomplete', 
                    [
                        'label' => 'Cliente',
                        'required' => true,
                        'property' => ['id', 'identifier', 'fullname'],
                        'placeholder' => 'Por favor, introduzca 3 caracteres. Puede buscar por Nif, Nombre y Id',
                    ]);
    }

    /**
     * Bloque de los datos de la factura
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormInvoice(FormMapper $formMapper)
    {
        $formMapper->add('date', 'sonata_type_date_picker', ['label' => 'Fecha']);
        $formMapper->add('paid', MoneyType::class, ['label' => 'Cantidad Pagada', 'currency' => 'EUR']);
        if (!empty($this->getSubject()) && !$this->getSubject()->isPaid()){
            $formMapper->add('is_paid', 'checkbox', ['label' => 'Pagada', 'required' => false]);
        }else{
            $formMapper->add('is_paid', 'checkbox', ['label' => 'Pagada', 'required' => false, 'disabled' => true]);
        }
        $formMapper->add('items', 'sonata_type_collection', 
                ['label' => 'Líneas', 'by_reference' => false], 
                ['edit' => 'inline', 'inline' => 'table', 'admin_code' => 'sonata.admin.order.invoice.item']);
        // solucion temporal para incluir los botones de añadir items,
        // lo hacemos con el campo vehicle pero sin mapear para poder añadir 
        // un autocomplete que tiene la opcion de template
        // @todo: buscar otra forma mas elegante de hacerlo
        $formMapper->add('aux.items', 'sonata_type_model_autocomplete', [
                'label' => ' ',
                'property' => ['id'],
                'template' => 'AppBundle:Admin:sonata_type_model_autocomplete_add_labor_part.html.twig',
                'required' => false,
                'mapped' => false,
            ],
            ['admin_code' => 'sonata.admin.order.invoice.item']);
        $formMapper->add('base', MoneyType::class, ['label' => 'Total', 'currency' => 'EUR', 'disabled' => true, 'required' => false]);
        $formMapper->add('tax', MoneyType::class, ['label' => 'IVA', 'currency' => 'EUR', 'disabled' => true, 'required' => false]);
        $formMapper->add('total', MoneyType::class, ['label' => 'Total (IVA incluido)', 'currency' => 'EUR', 'disabled' => true, 'required' => false]);
    }
    
    /**
     * Bloque para seleccionar la compañia
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormCompany(FormMapper $formMapper)
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        if (!$company){
            $formMapper->add('company', null, ['label' => 'Empresa']);
        }
    }
}

