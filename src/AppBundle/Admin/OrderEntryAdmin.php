<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use AppBundle\Form\Type\CustomerType;
use AppBundle\Form\Type\AddrType;
use AppBundle\Form\Type\VehicleType;

class OrderEntryAdmin extends Admin
{
    /**
     * Default form options
     *
     * @var array
     */
    protected $formOptions = [
        'trim' => true,
    ];

    /**
     * Default datagrid values
     *
     * @var array
     */
    protected $datagridValues = [
            '_page' => 1,            // display the first page (default = 1)
            '_sort_order' => 'DESC', // reverse order (default = 'ASC')
    ];
    
    public function getNewInstance()
    {
        $entity = parent::getNewInstance();
        $customer = false;
        $vehicle = false;
        $c_id = $this->getRequest()->query->get('cod_c', false);
        $v_id = $this->getRequest()->query->get('cod_v', false);
        if ($c_id){
            $customer = $this->getModelManager()->findOneBy('AppBundle:Customer', ['id' => $c_id]);
        }
        if ($v_id){
            $vehicle = $this->getModelManager()->findOneBy('AppBundle:Vehicle', ['id' => $v_id]);
        }
        if ($customer){
            $entity->setCustomer($customer);
        }
        if ($vehicle){
            $entity->setVehicle($vehicle);
        }
 
        return $entity;
    }
    
    /**
     * @todo: Pasar a un listener
     * @param object $object
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        if ($company){
            $object->setCompany($company);
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {            
        $entity = $this->getSubject();
        $customer = $entity->getCustomer();
        $vehicle = $entity->getVehicle();
        if ($entity->getId()){
            // EDITAR
            $this->addBlockFormEdit($formMapper);
        }elseif ($vehicle){
            // CREAR desde el link de vehiculos
            $this->addBlockFormCreateFromVehicle($formMapper, $vehicle);
        }elseif ($customer){
            // CREAR desde el link de clientes
            $this->addBlockFormCreateFromCustomer($formMapper, $customer);
        }else{
            // CREAR desde el link de agregar nueva y cualquier llamada POST de la creacion
            $this->addBlockFormCreate($formMapper);
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('number', null, ['label' => 'Número']);
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        if (!$company){
            $datagridMapper->add('company', null, ['label' => 'Empresa']);
        }
        $datagridMapper->add('customer_data.identifier', null, [
                'label' => 'Nif',
                'field_name' => 'customer_data.identifier'
            ]);
        $datagridMapper->add('customer_data.fullname', null, [
                'label' => 'Nombre',
                'field_name' => 'customer_data.fullname'
            ]);
        $datagridMapper->add('vehicle_data.registration', null, [
                'label' => 'Matricula',
                'field_name' => 'vehicle_data.registration'
            ]);
        $datagridMapper->add('date_in', 'doctrine_orm_date_range', 
                ['label' => 'Fecha de Entrada', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('date_out', 'doctrine_orm_date_range', 
                ['label' => 'Fecha de Salida', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('created_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Creado', 'field_type'=>'sonata_type_datetime_range_picker']);
        $datagridMapper->add('updated_at', 'doctrine_orm_datetime_range', 
                ['label' => 'Modificado', 'field_type'=>'sonata_type_datetime_range_picker']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('number', null, ['label' => 'Número']);
        
          //  Comentamos la compañia, deberia valer con el numero para distinguir la empresa
          //  @todo: eliminar si no solicitan volver a poner
//        $company = $this->getConfigurationPool()->getContainer()
//                ->get('security.token_storage')->getToken()
//                ->getUser()->getCompany();
//        if (!$company){
//            $listMapper->add('company', null, ['label' => 'Empresa']);
//        }
        
        $listMapper->add('customer_data.identifier', 'text', ['label' => 'Nif']);
        $listMapper->add('customer_data.fullname', null, ['label' => 'Nombre']);
        $listMapper->add('vehicle_data.registration', null, ['label' => 'Matricula']);
        $listMapper->add('km', null, ['label' => 'Km']);
        $listMapper->add('date_in', 'date', ['label' => 'Fecha de Entrada']);
        $listMapper->add('totalBudgets', 'string', ['label' => 'Total Presupuesto']);
        $listMapper->add('totalInvoices', 'string', ['label' => 'Total Factura']);
        $listMapper->add('_action', 'actions', ['actions' => [
                'show' => [],
                'edit' => [], 
                'delete' => [],
                'listActions' => [
                    'template' => 'AppBundle:Admin:listActionOrderEntry.html.twig'
                ],
                ]]);
    }
    
   protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('number', null, ['label' => 'Número']);
        $showMapper->add('company', null, ['label' => 'Empresa']);
        $showMapper->add('customer', null, ['label' => 'Id Cliente', 'associated_property' => 'id']);
        $showMapper->add('customer_data', null, ['label' => 'Cliente']);
        $showMapper->add('address_data', null, ['label' => 'Direccion']);
        $showMapper->add('vehicle', null, ['label' => 'Id Vehículo', 'associated_property' => 'id']);
        $showMapper->add('vehicle_data', null, ['label' => 'Vehiculo']);
        $showMapper->add('km', null, ['label' => 'Kms']);
        $showMapper->add('deposit', null, ['label' => 'Deposito']);
        $showMapper->add('date_in', 'date', ['label' => 'Fecha de entrada']);
        $showMapper->add('date_out', 'date', ['label' => 'Fecha de salida']);
        $showMapper->add('description', null, ['label' => 'Descripción']);
        $showMapper->add('created_at', null, ['label' => 'Creado']);
        $showMapper->add('updated_at', null, ['label' => 'Modificado']);
        $showMapper->add('actions' , null, [
                    'template' => 'AppBundle:Admin:showActionOrderEntry.html.twig'
                ]
        );
    }

    public function createQuery($context = 'list')
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();

        $query = parent::createQuery($context);
        if ($company && !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
            $query->andWhere($query->getRootAliases()[0] . '.company = :company');
            $query->setParameter('company', $company);
        }
        return $query;
    }

    
    /**
     * Formulario de creación
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormCreate(FormMapper $formMapper)
    {
        $this->addBlockFormCompany($formMapper);
        //Si llega como parametro la direccion agregamos el campo (caso de POST en los link desde vehiculo y cliente)
        $uniqid = $this->getRequest()->query->get('uniqid');
        $formData = $this->getRequest()->request->get($uniqid);
        if (isset($formData['address'])){
            $formMapper->add('address', 'sonata_type_model_hidden');
        }
        $this->addBlockFormVehicle($formMapper);
        $this->addBlockFormVehicleInfo($formMapper);
        $this->addBlockFormOE($formMapper);
    }

    /**
     * Formulario de creación desde el link de clientes
     *
     * @param FormMapper $formMapper
     * @param \AppBundle\Entity\Customer $customer
     */
    private function addBlockFormCreateFromCustomer(FormMapper $formMapper, \AppBundle\Entity\Customer $customer)
    {
        $this->addBlockFormCompany($formMapper);
        $this->addBlockFormAddress($formMapper, $customer);
        $this->addBlockFormVehicle($formMapper, $customer);
        $this->addBlockFormVehicleInfo($formMapper);
        $this->addBlockFormOE($formMapper);
    }

    /**
     * Formulario de creación desde el link de vehiculos
     *
     * @param FormMapper $formMapper
     * @param \AppBundle\Entity\Vehicle $vehicle
     */
    private function addBlockFormCreateFromVehicle(FormMapper $formMapper, \AppBundle\Entity\Vehicle $vehicle)
    {
        $this->addBlockFormCompany($formMapper);
        $this->addBlockFormAddress($formMapper, $vehicle->getCustomer());
        $this->addBlockFormVehicle($formMapper, $vehicle->getCustomer(), $vehicle);
        $this->addBlockFormVehicleInfo($formMapper);
        $this->addBlockFormOE($formMapper);
    }

    /**
     * Formulario de edición en tabs
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormEdit(FormMapper $formMapper)
    {
        $formMapper->tab('OE');
        $formMapper->with('');
        $this->addBlockFormCompany($formMapper);
        $this->addBlockFormOE($formMapper);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Cliente');
        $formMapper->with('');
        $formMapper->add('customer_data', CustomerType::class, ['label' => 'Cliente']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Dirección');
        $formMapper->with('');
        $formMapper->add('address_data',  AddrType::class, ['label' => 'Dirección']);
        $formMapper->end();
        $formMapper->end();
        $formMapper->tab('Vehículo');
        $formMapper->with('');
        $formMapper->add('vehicle_data',  VehicleType::class, ['label' => 'Vehículo']);
        $this->addBlockFormVehicleInfo($formMapper);
        $formMapper->end();
        $formMapper->end();
    }
    
    /**
     * Bloque para seleccionar la compañia
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormCompany(FormMapper $formMapper)
    {
        $company = $this->getConfigurationPool()->getContainer()
                ->get('security.token_storage')->getToken()
                ->getUser()->getCompany();
        if (!$company){
            $formMapper->add('company', null, ['label' => 'Empresa']);
        }
    }

    /**
     * Bloque para seleccionar la dirección
     *
     * @param FormMapper $formMapper
     * @param \AppBundle\Entity\Customer $customer
     */
    private function addBlockFormAddress(FormMapper $formMapper, $customer)
    {
        $formMapper->add('address', 'entity', 
                [
                    'required' => false,
                    'class' => 'AppBundle\Entity\Address',
                    'query_builder' => function($er) use ($customer){
                        return $er->createQueryBuilder('d')
                                ->where('d.customer = :customer')
                                ->setParameter('customer', $customer);
                    },
                ]
        );
    }

    /**
     * Bloque para seleccionar el vehículo
     *
     * @param FormMapper $formMapper
     * @param \AppBundle\Entity\Customer $customer
     * @param \AppBundle\Entity\Vehicle $vehicle
     */
    private function addBlockFormVehicle(FormMapper $formMapper, $customer = null, $vehicle = null)
    {
        if ($customer || $vehicle){
            $formMapper->add('vehicle', 'entity', 
                    [
                        'class' => 'AppBundle\Entity\Vehicle',
                        'query_builder' => function($er) use ($customer, $vehicle){
                            if ($vehicle){
                                return $er->createQueryBuilder('v')
                                        ->where('v.id = :vehicle')
                                        ->setParameter('vehicle', $vehicle);              
                            }elseif ($customer){
                                return $er->createQueryBuilder('v')
                                        ->where('v.customer = :customer')
                                        ->setParameter('customer', $customer);                      
                            }
                        }, 
                        'property' => 'customerVehicle',
                    ]
            );
        }else{
            $formMapper->add('vehicle', 'sonata_type_model_autocomplete', 
                    [
                        'property' => ['registration'],
                        'placeholder' => 'Por favor, introduzca 3 caracteres. Puede buscar por Matricula, Nif, Nombre y Id',
                        'callback' => function ($admin, $property, $value) {
                            $datagrid = $admin->getDatagrid();
                            $queryBuilder = $datagrid->getQuery();
                            $queryBuilder
                                ->innerJoin($queryBuilder->getRootAlias() . '.customer', 'c')
                                ->orWhere($queryBuilder->getRootAlias() . '.id = :barValue')
                                ->orWhere($queryBuilder->getRootAlias() . '.registration like :likeValue')
                                ->orWhere('c.id = :barValue')
                                ->orWhere('c.identifier like :likeValue')
                                ->orWhere('c.fullname like :likeValue')
                                ->setParameter('barValue', $value)
                                ->setParameter('likeValue', '%'.$value.'%')
                            ;
                        },
                        'to_string_callback' => function($entity, $property) {
                            return $entity->getCustomerVehicle();
                        },
                    ]
            );
        }
    }

    /**
     * Bloque para seleccionar la información del vehículo
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormVehicleInfo(FormMapper $formMapper)
    {
        $formMapper->add('km', null, ['label' => 'Kms']);
        $formMapper->add('deposit', 'percent', ['label' => 'Deposito', 'required' => false]);
    }

    /**
     * Bloque de los datos de la OE
     *
     * @param FormMapper $formMapper
     */
    private function addBlockFormOE(FormMapper $formMapper)
    {
        $formMapper->add('date_in', 'sonata_type_date_picker', ['label' => 'Fecha de Entrada']);
        $formMapper->add('date_out', 'sonata_type_date_picker', ['label' => 'Fecha de Salida', 'required' => false]);
        $formMapper->add('description', null, ['label' => 'Descrición']);
    }
}

