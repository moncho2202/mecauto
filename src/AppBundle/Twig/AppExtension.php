<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('is_labor', [$this, 'isLabor']),
            new \Twig_SimpleFunction('is_part', [$this, 'isPart']),
        );
    }
    
    public function isLabor($object){
        return ($object instanceof \AppBundle\Entity\Labor);
    }
    public function isPart($object){
        return ($object instanceof \AppBundle\Entity\Part);
    }

    public function getName()
    {
        return 'app_extension';
    }
}
