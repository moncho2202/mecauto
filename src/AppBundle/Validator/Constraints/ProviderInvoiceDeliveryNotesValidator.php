<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProviderInvoiceDeliveryNotesValidator extends ConstraintValidator
{
    public function validate($entity, Constraint $constraint)
    {
        // @TODO Put translations
        
        foreach ($entity->getDeliveryNotes() as $deliveryNote){
            if ($deliveryNote->getProvider()->getId() != $entity->getProvider()->getId()){
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%string%', "Hay asociado albarenes que no corresponden con este proveedor")
                    ->atPath('provider')
                    ->addViolation();
            }
        }
    }
}

