<?php
namespace AppBundle\Validator\Constraints;

class ToolKit
{
    public static function isNifNieValid($nif)
    {
        $resultado = self::validNifCif($nif);
        if ($resultado == 1 || $resultado == 3) {
            return true;
        } else {
            return false;
        }
    }

    public static function isNifValid($nif)
    {
        $resultado = self::validNifCif($nif);
        if ($resultado == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function isCifValid($cif)
    {
        $resultado = self::validNifCif($cif);
        if ($resultado == 2) {
            return true;
        } else {
            return false;
        }
    }

    public static function isNieValid($nie)
    {
        $resultado = self::validNifCif($nie);
        if ($resultado == 3) {
            return true;
        } else {
            return false;
        }
    }

    public static function validNifCif($nifCif)
    {
        // Returns: 1 = NIF ok, 2 = CIF ok, 3 = NIE ok, -1 = NIF bad, -2 = CIF bad, -3 = NIE bad, 0 = ??? bad
        $nifCif = strtoupper($nifCif);
        for ($i = 0; $i < 9; $i ++) {
            $num[$i] = substr($nifCif, $i, 1);
        }
        // si no tiene un formato valido devuelve error
        if (! preg_match('/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/', $nifCif)) {
            return 0;
        }
        // comprobacion de NIFs estandar
        if (preg_match('/(^[0-9]{8}[A-Z]{1}$)/', $nifCif)) {
            if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($nifCif, 0, 8) % 23, 1)) {
                return 1;
            } else {
                return - 1;
            }
        }
        // algoritmo para comprobacion de codigos tipo CIF
        $suma = $num[2] + $num[4] + $num[6];
        for ($i = 1; $i < 8; $i += 2) {
            $suma += substr((2 * $num[$i]), 0, 1) + substr((2 * $num[$i]), 1, 1);
        }
        $n = 10 - substr($suma, strlen($suma) - 1, 1);
        // comprobacion de NIFs especiales (se calculan como CIFs o como NIFs)
        if (preg_match('/^[KLM]{1}/', $nifCif)) {
            if ($num[8] == chr(64 + $n) || $num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($nifCif, 1, 8) % 23, 1)) {
                return 1;
            } else {
                return - 1;
            }
        }
        // comprobacion de CIFs
        if (preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $nifCif)) {
            if ($num[8] == chr(64 + $n) || $num[8] == substr($n, strlen($n) - 1, 1)) {
                return 2;
            } else {
                return - 2;
            }
        }
        // comprobacion de NIEs
        if (preg_match('/^[XYZ]{1}/', $nifCif)) {
            if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(array(
                'X',
                'Y',
                'Z'
            ), array(
                '0',
                '1',
                '2'
            ), $nifCif), 0, 8) % 23, 1)) {
                return 3;
            } else {
                return - 3;
            }
        }
        // si todavia no se ha verificado devuelve error
        return 0;
    }
}
