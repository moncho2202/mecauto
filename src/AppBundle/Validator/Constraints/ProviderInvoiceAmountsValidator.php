<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProviderInvoiceAmountsValidator extends ConstraintValidator
{
    public function validate($entity, Constraint $constraint)
    {
        // @TODO Put translations
        
        if ($entity->getBaseTax() !== $entity->getTotal()){
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', "La suma de la Base más el IVA no cuadra con el total")
                ->atPath('total')
                ->addViolation();
        }

        if (abs($entity->getTax()) > abs($entity->getBase())){
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', "El IVA no puede ser mayor que la Base")
                ->atPath('tax')
                ->addViolation();
        }
    }
}

