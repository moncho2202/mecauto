<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Nif extends Constraint
{
    public $message = 'El valor "%string%" no es un NIF valido.';
    
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}

