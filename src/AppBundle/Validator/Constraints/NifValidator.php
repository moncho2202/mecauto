<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NifValidator extends ConstraintValidator
{
    const FORCE_NIF_KEY = '%F%';

    public function validate($value, Constraint $constraint)
    {
        if (0 !== strpos($value, self::FORCE_NIF_KEY) && ToolKit::validNifCif($value) <= 0) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}

