<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PfjValidator extends ConstraintValidator
{
    public function validate($entity, Constraint $constraint)
    {
        // @TODO Put translations
        
        if (ToolKit::isNifNieValid($entity->getIdentifier()) && !$entity->getSurname1()) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', "Para personas físicas es obligatorio el primer apellido")
                ->atPath('apellido1')
                ->addViolation();
        }
        
        if (ToolKit::isCifValid($entity->getIdentifier())){
            if ($entity->getSurname1()){
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%string%', "Las personas juridicas no llevan primer apellido")
                    ->atPath('apellido1')
                    ->addViolation();
            }
                
            if ($entity->getSurname2()){
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%string%', "Las personas juridicas no llevan segundo apellido")
                    ->atPath('apellido2')
                    ->addViolation();
            }
        }
    }
}

