<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ProviderInvoiceDeliveryNotes extends Constraint
{
    public $message = '%string%';
    
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}

