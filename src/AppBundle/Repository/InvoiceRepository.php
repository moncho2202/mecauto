<?php

namespace AppBundle\Repository;

/**
 * InvoiceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InvoiceRepository extends \Doctrine\ORM\EntityRepository
{
    public function findMaxNumberByCompany(\AppBundle\Entity\Company $company, $year = null)
    {
        return $this->getEntityManager()->getRepository('AppBundle:bill')->findMaxNumberByCompany($company, $year);
    }
}
