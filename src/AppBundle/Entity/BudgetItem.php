<?php

namespace AppBundle\Entity;

/**
 * BudgetItem
 */
class BudgetItem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $discount;

    /**
     * @var string
     */
    private $tax;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var string
     */
    private $price;

    /**
     * @var \AppBundle\Entity\Budget
     */
    private $budget;

    /**
     * @var \AppBundle\Entity\Item
     */
    private $item;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return BudgetItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return BudgetItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return BudgetItem
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return BudgetItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return BudgetItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set budget
     *
     * @param \AppBundle\Entity\Budget $budget
     *
     * @return BudgetItem
     */
    public function setBudget(\AppBundle\Entity\Budget $budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return \AppBundle\Entity\Budget
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Item $item
     *
     * @return BudgetItem
     */
    public function setItem(\AppBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    public function __toString()
    {
        return $this->getItem()." (Cantidad: {$this->getAmount()} - Precio: {$this->getPrice()})";
    }

    /**
     * Get Total Price value witout discount and without tax (euros) 
     * 
     * @return decimal
     */
    public function getPriceTotal()
    {
        return round($this->amount * $this->price,2);
    }

    /**
     * Get Discount value (euros)
     * 
     * @return decimal
     */
    public function getDiscountValue()
    {
        return round($this->getPriceTotal() * $this->discount, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return decimal
     */
    public function getTaxValue()
    {
        return $this->getBase() * $this->tax;
    }

    /**
     * Get Total with discount and without tax (euros)
     * 
     * @return decimal
     */
    public function getBase()
    {
        return round($this->getPriceTotal() - $this->getDiscountValue(), 2);
    }

    /**
     * Get Total with discount and with tax (euros)
     * 
     * @return decimal
     */
    public function getTotal()
    {
        return round($this->getBase() + $this->getTaxValue(), 2);
    }
}
