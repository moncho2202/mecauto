<?php

namespace AppBundle\Entity;

/**
 * CustomerData
 */
class CustomerData
{
    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname1;

    /**
     * @var string
     */
    private $surname2;

    /**
     * @var string
     */
    private $fullname;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $movil;


    public function __construct(Customer $customer = null)
    {
        if ($customer){
            $this->setData($customer);
        }
    }

    public function setData(Customer $customer)
    {
        $this->setName($customer->getName());
        $this->setSurname1($customer->getSurname1());
        $this->setSurname2($customer->getSurname2());
        $this->setIdentifier($customer->getIdentifier());
        $this->setFullname($customer->getFullname());
        $this->setPhone($customer->getPhone());
        $this->setMovil($customer->getMovil());
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     *
     * @return CustomerData
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustomerData
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->setFullname();

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname1
     *
     * @param string $surname1
     *
     * @return CustomerData
     */
    public function setSurname1($surname1)
    {
        $this->surname1 = $surname1;
        $this->setFullname();

        return $this;
    }

    /**
     * Get surname1
     *
     * @return string
     */
    public function getSurname1()
    {
        return $this->surname1;
    }

    /**
     * Set surname2
     *
     * @param string $surname2
     *
     * @return CustomerData
     */
    public function setSurname2($surname2)
    {
        $this->surname2 = $surname2;
        $this->setFullname();

        return $this;
    }

    /**
     * Get surname2
     *
     * @return string
     */
    public function getSurname2()
    {
        return $this->surname2;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return CustomerData
     */
    public function setFullname($fullname = null)
    {
        if (is_null($fullname)){
            $this->fullname = trim($this->getName()." ".$this->getSurname1()." ".$this->getSurname2());
        }else{
            $this->fullname = $fullname;
        }

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return CustomerData
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set movil
     *
     * @param string $movil
     *
     * @return CustomerData
     */
    public function setMovil($movil)
    {
        $this->movil = $movil;

        return $this;
    }

    /**
     * Get movil
     *
     * @return string
     */
    public function getMovil()
    {
        return $this->movil;
    }
    
    public function __toString()
    {
        return $this->getFullname();
    }
}
