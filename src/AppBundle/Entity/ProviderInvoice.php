<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * ProviderInvoice
 */
class ProviderInvoice
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $base;

    /**
     * @var string
     */
    private $tax;

    /**
     * @var string
     */
    private $total;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var bool
     */
    private $paid;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \AppBundle\Entity\Provider
     */
    private $provider;

    /**
     * @var ArrayCollection
     */
    private $deliveryNotes;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->deliveryNotes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return ProviderInvoice
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set base
     *
     * @param string $base
     *
     * @return ProviderInvoice
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return round($this->base, 2);
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return ProviderInvoice
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return round($this->tax, 2);
    }

    /**
     * Get base + tax
     *
     * @return string
     */
    public function getBaseTax()
    {
        return round($this->getBase()+$this->getTax(), 2);
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ProviderInvoice
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return round($this->total, 2);
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ProviderInvoice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set paid
     *
     * @param boolean $paid
     *
     * @return ProviderInvoice
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return bool
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProviderInvoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set provider
     *
     * @param \AppBundle\Entity\Provider $provider
     *
     * @return ProviderInvoice
     */
    public function setProvider(\AppBundle\Entity\Provider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AppBundle\Entity\Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Add deliveryNote
     *
     * @param \AppBundle\Entity\DeliveryNote $deliveryNote
     *
     * @return ProviderInvoice
     */
    public function addDeliveryNote(\AppBundle\Entity\DeliveryNote $deliveryNote)
    {
        $deliveryNote->setInvoice($this);
        $this->deliveryNotes[] = $deliveryNote;

        return $this;
    }

    /**
     * Remove deliveryNote
     *
     * @param \AppBundle\Entity\DeliveryNote $deliveryNote
     */
    public function removeDeliveryNote(\AppBundle\Entity\DeliveryNote $deliveryNote)
    {
        $this->deliveryNotes->removeElement($deliveryNote);
        $deliveryNote->setInvoice(null);
    }

    /**
     * Get deliveryNotes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeliveryNotes()
    {
        return $this->deliveryNotes;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setValuesEmpty();
    }

    public function preUpdate()
    {
        $this->setValuesEmpty();
    }

    /**
     * Set values empty in prePersist y preUpdate
     */
    private function setValuesEmpty()
    {
        if (is_null($this->base)){
            $this->setBase($this->getBaseFromDeliveryNotes());
        }
        if (is_null($this->tax)){
            $this->setTax($this->getTaxFromDeliveryNotes());
        }
        if (is_null($this->total)){
            $this->setTotal($this->getBase()+$this->getTax());
        }
    }

    public function __toString()
    {
        return $this->getNumber();
    }

    /**
     * Get base from delivery notes 
     *
     * @return \AppBundle\Entity\ProviderInvoice
     */
    public function getBaseFromDeliveryNotes()
    {
        $base = 0;
        foreach ($this->getDeliveryNotes() as $deliveryNote)
        {
            $base += $deliveryNote->getBase();
        }
        return $base;
    }

    /**
     * Get tax from delivery notes 
     *
     * @return decimal
     */
    public function getTaxFromDeliveryNotes()
    {
        $tax = 0;
        foreach ($this->getDeliveryNotes() as $deliveryNote)
        {
            $tax += $deliveryNote->getTax();
        }
        return $tax;
    }

    /**
     * Get tax from delivery notes 
     *
     * @return decimal
     */
    public function getTotalFromDeliveryNotes()
    {
        $total = 0;
        foreach ($this->getDeliveryNotes() as $deliveryNote)
        {
            $total += $deliveryNote->getTotal();
        }
        return $total;
    }
}

