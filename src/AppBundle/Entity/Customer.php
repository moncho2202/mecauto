<?php

namespace AppBundle\Entity;

use AppBundle\Validator\Constraints\NifValidator;
use AppBundle\Validator\Constraints\ToolKit;

/**
 * Customer
 */
class Customer
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname1;

    /**
     * @var string
     */
    private $surname2;

    /**
     * @var string
     */
    private $fullname;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $movil;

    /**
     * @var string
     */
    private $note;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $address;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $vehicles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->address = new \Doctrine\Common\Collections\ArrayCollection();
        $this->vehicles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Customer
     */
    public function setType($type)
    {
        $this->type = strtoupper($type);

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Get type descripcion
     *
     * @return string 
     */
    public function getTypeDesc()
    {
        return self::getTypes()[strtoupper($this->type)];
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     *
     * @return Customer
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
        
        if (ToolKit::isNifNieValid($identifier)) {
            $this->setType('PF');
        } elseif (ToolKit::isCifValid($identifier)) {
            $this->setType('PJ');
        } else {
            $this->setType('NA');
        }

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->setFullname();

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname1
     *
     * @param string $surname1
     *
     * @return Customer
     */
    public function setSurname1($surname1)
    {
        $this->surname1 = $surname1;
        $this->setFullname();

        return $this;
    }

    /**
     * Get surname1
     *
     * @return string
     */
    public function getSurname1()
    {
        return $this->surname1;
    }

    /**
     * Set surname2
     *
     * @param string $surname2
     *
     * @return Customer
     */
    public function setSurname2($surname2)
    {
        $this->surname2 = $surname2;
        $this->setFullname();

        return $this;
    }

    /**
     * Get surname2
     *
     * @return string
     */
    public function getSurname2()
    {
        return $this->surname2;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Customer
     */
    public function setFullname($fullname = null)
    {
        if (is_null($fullname)){
            $this->fullname = trim($this->getName()." ".$this->getSurname1()." ".$this->getSurname2());
        }else{
            $this->fullname = $fullname;
        }

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set movil
     *
     * @param string $movil
     *
     * @return Customer
     */
    public function setMovil($movil)
    {
        $this->movil = $movil;

        return $this;
    }

    /**
     * Get movil
     *
     * @return string
     */
    public function getMovil()
    {
        return $this->movil;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Customer
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Customer
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Customer
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return Customer
     */
    public function addAddress(\AppBundle\Entity\Address $address)
    {
        $address->setCustomer($this);
        $this->address[] = $address;

        return $this;
    }
    public function addAddres(\AppBundle\Entity\Address $address)
    {
        $this->addAddress($address);
    }

    /**
     * Remove address
     *
     * @param \AppBundle\Entity\Address $address
     */
    public function removeAddress(\AppBundle\Entity\Address $address)
    {
        $this->address->removeElement($address);
    }
    public function removeAddres(\AppBundle\Entity\Address $address)
    {
        $this->removeAddress($address);
    }


    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddress()
    {
        return $this->address;
    }
    
     /**
     * Get default address
     *
     * @return Address
     */
    public function getAddressDefault()
    {
        if (!$this->getAddress()){
            return null;
        }
        foreach ($this->getAddress() as $addr){
            if ($addr->isDefault()){
                return $addr;
            }
        }
        // If there is no default it returns the first
        return $this->getAddress()->first();
    }

    /**
     * Add vehicle
     *
     * @param \AppBundle\Entity\Vehicle $vehicle
     *
     * @return Customer
     */
    public function addVehicle(\AppBundle\Entity\Vehicle $vehicle)
    {
        $vehicle->setCustomer($this);
        $this->vehicles[] = $vehicle;

        return $this;
    }

    /**
     * Remove vehicle
     *
     * @param \AppBundle\Entity\Vehicle $vehicle
     */
    public function removeVehicle(\AppBundle\Entity\Vehicle $vehicle)
    {
        $this->vehicles->removeElement($vehicle);
    }

    /**
     * Get vehicles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    private function clearForcedIdentifier()
    {
        if (strpos($this->identifier, NifValidator::FORCE_NIF_KEY) === 0) {
            $this->identifier = substr($this->identifier, 3);
        }
    }

    public function prePersist()
    {
        $this->clearForcedIdentifier();
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->clearForcedIdentifier();
        $this->setUpdatedAt(new \DateTime());
    }
    
    public function __toString() {
        return $this->getFullname();
    }
    
    public static function getTypes()
    {
        return ['PF' => 'Persona Física', 'PJ' => 'Persona Jurídica'];
    }
}
