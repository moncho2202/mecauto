<?php

namespace AppBundle\Entity;

/**
 * OrderRepair
 */
class OrderRepair
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $sequence;

    /**
     * @var string
     */
    private $number;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \AppBundle\Entity\OrderEntry
     */
    private $oe;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return OrderRepair
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        $prefix = $this->oe->getCompany()->getPrefixOr();
        $this->setNumber($prefix.date('y').str_pad($sequence, 5, "00000", STR_PAD_LEFT));
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSecuence()
    {
        return $this->sequence;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return OrderRepair
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return OrderRepair
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get fecha
     *
     * @return string 
     */
    public function getDateFormat()
    {
        return $this->date->format('d-m-Y');
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return OrderRepair
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderRepair
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return OrderRepair
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set oe
     *
     * @param \AppBundle\Entity\OrderEntry $oe
     *
     * @return OrderRepair
     */
    public function setOe(\AppBundle\Entity\OrderEntry $oe)
    {
        $this->oe = $oe;

        return $this;
    }

    /**
     * Get oe
     *
     * @return \AppBundle\Entity\OrderEntry
     */
    public function getOe()
    {
        return $this->oe;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function __toString() {
        return trim($this->getId()." ".(($this->getOe())?$this->getOe()->getVehicle()->getRegistration()." ".$this->getOe()->getCustomer()->getIdentifier():"")." ".(($this->getDate())?$this->getDateFormat():""));
    }

    public function copyBudget(Budget $budget)
    {
        $this->setOe($budget->getOe());
        $description = "";
        foreach ($budget->getItems() as $budgetItem){
            $description .= $budgetItem->getAmount()." ".$budgetItem->getItem()->getDescription()."\n";
        }
        $this->setDescription($description);
    }
}
