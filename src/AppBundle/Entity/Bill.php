<?php /** @noinspection PhpUnused */

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Bill
 */
abstract class Bill
{
    /**
     * @var integer
     */
    protected $id;
   
    /**
     * @var integer
     */
    protected $sequence;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $paid = 0;
    
    /**
     * @var float
     */
    protected $total;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var CustomerData
     */
    protected $customer_data;

    /**
     * @var Address
     */
    protected $address;

    /**
     * @var AddressData
     */
    protected $address_data;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var DateTime
     */
    protected $created_at;

    /**
     * @var DateTime
     */
    protected $updated_at;

    /**
     * @var Collection
     */
    protected $items;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new DateTime();
        $this->items = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return Bill
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        $prefix = $this->getCompany()->getPrefixInvoice();
        $this->setNumber($prefix.date('y').str_pad($sequence, 5, "00000", STR_PAD_LEFT));

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSecuence()
    {
        return $this->sequence;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Bill
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set paid
     *
     * @param string $paid
     *
     * @return Bill
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return string
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set is paid
     *
     * @param boolean $paid
     *
     * @return Bill
     */
    public function setIsPaid($paid)
    {
        if ($paid){
            $this->setPaid($this->getTotal());
        }

        return $this;
    }

    /**
     * Get is paid
     *
     * @return bool
     */
    public function getIsPaid()
    {
        return abs($this->getPaid()) >= abs($this->getTotal());
    }
    public function isPaid()
    {
        return $this->getIsPaid();
    }

    /**
     * Set date
     *
     * @param DateTime $date
     *
     * @return Bill
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return Bill
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        if (!$this->getAddress() && $customer->getAddressDefault()){
            $this->setAddress($customer->getAddressDefault());
        }

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set customer
     *
     * @param CustomerData $customerData
     *
     * @return Bill
     */
    public function setCustomerData(CustomerData $customerData)
    {
        $this->customer_data = $customerData;

        return $this;
    }

    /**
     * Get customer
     *
     * @return CustomerData
     */
    public function getCustomerData()
    {
        return $this->customer_data;
    }

    /**
     * Set address
     *
     * @param Address $address
     *
     * @return Bill
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address
     *
     * @param AddressData $addressData
     *
     * @return Bill
     */
    public function setAddressData(AddressData $addressData)
    {
        $this->address_data = $addressData;

        return $this;
    }

    /**
     * Get address
     *
     * @return AddressData
     */
    public function getAddressData()
    {
        return $this->address_data;
    }

    /**
     * Set company
     *
     * @param Company $company
     * @return Bill
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
        
        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Bill
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Bill
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add item
     *
     * @param InvoiceItem $item
     *
     * @return Bill
     */
    public function addItem(InvoiceItem $item)
    {
        $this->items[] = $item;
        $item->setInvoice($this);
        
        return $this;
    }

    /**
     * Remove item
     *
     * @param InvoiceItem $item
     */
    public function removeItem(InvoiceItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
        $this->total = $this->getTotal();
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new DateTime());
        $this->total = $this->getTotal();
    }

    public function __toString()
    {
        return (string) $this->getNumber();
    }

    /**
     * Alias get customer identifier for sonata (embedded not working)
     * 
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getCustomerData()->getIdentifier();
    }

    /**
     * Alias get customer fullname for sonata (embedded not working)
     * 
     * @return string
     */
    public function getFullname()
    {
        return $this->getCustomerData()->getFullname();
    }

   /**
     * Get Total with discount and without tax (euros) only parts
     * 
     * @return float
     */
    public function getBaseParts()
    {
        $base = 0;
        foreach ($this->getItems() as $item){
            if ($item->getItem() instanceof Part){
                $base += $item->getBase();
            }
        }

        return round($base, 2);
    }

    /**
     * Get Total with discount and without tax (euros) only labors
     * 
     * @return float
     */
    public function getBaseLabors()
    {
        $base = 0;
        foreach ($this->getItems() as $item){
            if ($item->getItem() instanceof Labor){
                $base += $item->getBase();
            }
        }

        return round($base, 2);
    }
    
    /**
     * Get Total with discount and without tax (euros)
     * 
     * @return float
     */
    public function getBase()
    {
        $base = 0;
        foreach ($this->getItems() as $item){
            $base += $item->getBase();
        }

        return round($base, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return float
     */
    public function getTax()
    {
        $tax = 0;
        foreach ($this->getItems() as $item){
            $tax += $item->getTaxValue();
        }

        return round($tax, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return float
     */
    public function getTotal()
    {
        $total = $this->getBase()+$this->getTax();
        
        return round($total, 2);
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        if ($this instanceof Invoice){
            return 'Factura';
        }elseif ($this instanceof InvoiceCash){
            return 'Factura Contado';
        }elseif ($this instanceof InvoiceRefund){
            return 'Abono Factura';
        }elseif ($this instanceof InvoiceCashRefund){
            return 'Abono Factura Contado';
        }

        return null;
    }
}
