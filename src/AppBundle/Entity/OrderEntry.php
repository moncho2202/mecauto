<?php

namespace AppBundle\Entity;

/**
 * OrderEntry
 */
class OrderEntry
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $sequence;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var integer
     */
    protected $km;

    /**
     * @var string
     */
    protected $deposit;

    /**
     * @var \DateTime
     */
    protected $date_in;

    /**
     * @var \DateTime
     */
    protected $date_out;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * @var \DateTime
     */
    protected $updated_at;

    /**
     * @var \AppBundle\Entity\OrderRepair
     */
    protected $or;

    /**
     * @var \AppBundle\Entity\Company
     */
    protected $company;

    /**
     * @var \AppBundle\Entity\Customer
     */
    protected $customer;

    /**
     * @var \AppBundle\Entity\Vehicle
     */
    protected $vehicle;

    /**
     * @var \AppBundle\Entity\Address
     */
    protected $address;

    /**
     * @var \AppBundle\Entity\CustomerData
     * 
     * @todo analizar cambiar nombre
     */
    protected $customer_data;

    /**
     * @var \AppBundle\Entity\VehicleData
     * 
     * @todo analizar cambiar nombre
     */
    protected $vehicle_data;

    /**
     * @var \AppBundle\Entity\AddressData
     * 
     * @todo analizar cambiar nombre
     */
    protected $address_data;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $budgets;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $invoices;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date_in = new \DateTime();
        $this->budgets = new \Doctrine\Common\Collections\ArrayCollection();
        $this->invoices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return OrderEntry
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        $prefix = $this->getCompany()->getPrefixOe();
        $this->setNumber($prefix.date('y').str_pad($sequence, 5, "00000", STR_PAD_LEFT));
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSecuence()
    {
        return $this->sequence;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return OrderEntry
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set km
     *
     * @param integer $km
     *
     * @return OrderEntry
     */
    public function setKm($km)
    {
        $this->km = $km;

        return $this;
    }

    /**
     * Get km
     *
     * @return integer
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set deposit
     *
     * @param string $deposit
     *
     * @return OrderEntry
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return string
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set dateIn
     *
     * @param \DateTime $dateIn
     *
     * @return OrderEntry
     */
    public function setDateIn($dateIn)
    {
        $this->date_in = $dateIn;

        return $this;
    }

    /**
     * Get dateIn
     *
     * @return \DateTime
     */
    public function getDateIn()
    {
        return $this->date_in;
    }

    /**
     * Get dateIn
     *
     * @return string 
     */
    public function getDateInFormat()
    {
        return $this->date_in->format('d-m-Y');
    }

    /**
     * Set dateOut
     *
     * @param \DateTime $dateOut
     *
     * @return OrderEntry
     */
    public function setDateOut($dateOut)
    {
        $this->date_out = $dateOut;

        return $this;
    }

    /**
     * Get dateOut
     *
     * @return \DateTime
     */
    public function getDateOut()
    {
        return $this->date_out;
    }
    
    /**
     * Get dateIn
     *
     * @return string 
     */
    public function getDateOutFormat()
    {
        return $this->date_out->format('d-m-Y');
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return OrderEntry
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return OrderEntry
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set or
     *
     * @param \AppBundle\Entity\OrderRepair $or
     *
     * @return OrderEntry
     */
    public function setOr(\AppBundle\Entity\OrderRepair $or = null)
    {
        $this->or = $or;

        return $this;
    }

    /**
     * Get or
     *
     * @return \AppBundle\Entity\OrderRepair
     */
    public function getOr()
    {
        return $this->or;
    }

    /**
     * Has or
     * 
     * @return boolean
     */
    public function hasOr()
    {
        return (boolean) $this->or;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return OrderEntry
     */
    public function setCompany(\AppBundle\Entity\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\Customer $customer
     *
     * @return OrderEntry
     */
    public function setCustomer(\AppBundle\Entity\Customer $customer)
    {
        $this->customer = $customer;
        if (!$this->getAddress() && $customer->getAddressDefault()){
            $this->setAddress($customer->getAddressDefault());
        }

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set vehicle
     *
     * @param \AppBundle\Entity\Vehicle $vehicle
     *
     * @return OrderEntry
     */
    public function setVehicle(\AppBundle\Entity\Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
        $this->setCustomer($vehicle->getCustomer());
        
        return $this;
    }

    /**
     * Get vehicle
     *
     * @return \AppBundle\Entity\Vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return OrderEntry
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set customerData
     *
     * @param \AppBundle\Entity\CustomerData $customerData
     *
     * @return OrderEntry
     */
    public function setCustomerData(\AppBundle\Entity\CustomerData $customerData)
    {
        $this->customer_data = $customerData;

        return $this;
    }

    /**
     * Get customerData
     *
     * @return \AppBundle\Entity\CustomerData
     */
    public function getCustomerData()
    {
        return $this->customer_data;
    }

    /**
     * Set vehicleData
     *
     * @param \AppBundle\Entity\VehicleData $vehicleData
     *
     * @return OrderEntry
     */
    public function setVehicleData(\AppBundle\Entity\VehicleData $vehicleData)
    {
        $this->vehicle_data = $vehicleData;
        
        return $this;
    }

    /**
     * Get vehicleData
     *
     * @return \AppBundle\Entity\VehicleData
     */
    public function getVehicleData()
    {
        return $this->vehicle_data;
    }

    /**
     * Set addressData
     *
     * @param \AppBundle\Entity\AddressData $addressData
     *
     * @return OrderEntry
     */
    public function setAddressData(\AppBundle\Entity\AddressData $addressData = null)
    {
        $this->address_data = $addressData;

        return $this;
    }

    /**
     * Get addressData
     *
     * @return \AppBundle\Entity\AddressData
     */
    public function getAddressData()
    {
        return $this->address_data;
    }

    /**
     * Add budget
     *
     * @param \AppBundle\Entity\Budget $budget
     *
     * @return OrderEntry
     */
    public function addBudget(\AppBundle\Entity\Budget $budget)
    {
        $this->budgets[] = $budget;

        return $this;
    }

    /**
     * Remove budget
     *
     * @param \AppBundle\Entity\Budget $budget
     */
    public function removeBudget(\AppBundle\Entity\Budget $budget)
    {
        $this->budgets->removeElement($budget);
    }

    /**
     * Get budgets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBudgets()
    {
        return $this->budgets;
    }
    
    /**
     * Has Budget
     * 
     * @return boolean
     */
    public function hasBudget()
    {
        return (boolean) $this->budgets->count();
    }

    /**
     * Add invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return OrderEntry
     */
    public function addInvoice(\AppBundle\Entity\Invoice $invoice)
    {
        $this->invoices[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     */
    public function removeInvoice(\AppBundle\Entity\Invoice $invoice)
    {
        $this->invoices->removeElement($invoice);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Has Invoice
     * 
     * @return boolean
     */
    public function hasInvoice()
    {
        return (boolean) $this->invoices->count();
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function __toString() {
        return trim($this->getId()." ".(($this->getVehicle())?$this->getVehicle()->getCustomerVehicle():"")." ".(($this->getDateIn())?$this->getDateInFormat():""));
    }

    /**
     * Alias get customer identifier for sonata (embedded not working)
     * 
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getCustomerData()->getIdentifier();
    }

    /**
     * Alias get customer fullname for sonata (embedded not working)
     * 
     * @return string
     */
    public function getFullname()
    {
        return $this->getCustomerData()->getFullname();
    }

   /**
     * Alias get vehicle registration for sonata (embedded not working)
     * 
     * @return string
     */
    public function getRegistration()
    {
        return $this->getVehicleData()->getRegistration();
    }

    /**
     * Get Totals of invoices associated to OE
     * 
     * @return string
     */
    public function getTotalInvoices()
    {
        $total = [];
        foreach ($this->getInvoices() as $invoice){
            $total[] = number_format($invoice->getTotal(),2,",",".")." €";
        }
        
        return implode($total, ", ");
    }    /**
     * Get Totals of budgets associated to OE
     * 
     * @return string
     */
    public function getTotalBudgets()
    {
        $total = [];
        foreach ($this->getBudgets() as $budget){
            $total[] = number_format($budget->getTotal(),2,",",".")." €";
        }
        
        return implode($total, ", ");
    }


}
