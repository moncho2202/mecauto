<?php

namespace AppBundle\Entity;

/**
 * InvoiceItem
 */
class InvoiceItem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $discount;

    /**
     * @var string
     */
    private $tax;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var string
     */
    private $price;

    /**
     * @var \AppBundle\Entity\Bill
     * 
     * Se mantiene el nombre de invoice pero apunta a bill para usar la misma 
     * tabla para todas las tablas que heredan de bill
     */
    private $invoice;

    /**
     * @var \AppBundle\Entity\Item
     */
    private $item;

    /**
     * @var \AppBundle\Entity\Output
     */
    private $output;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InvoiceItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return InvoiceItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return InvoiceItem
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return InvoiceItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return InvoiceItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\Bill $invoice
     *
     * @return InvoiceItem
     */
    public function setInvoice(\AppBundle\Entity\Bill $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Item $item
     *
     * @return InvoiceItem
     */
    public function setItem(\AppBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set output
     *
     * @param \AppBundle\Entity\Output $output
     *
     * @return InvoiceItem
     */
    public function setOutput(\AppBundle\Entity\Output $output = null)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Get output
     *
     * @return \AppBundle\Entity\Output
     */
    public function getOutput()
    {
        return $this->output;
    }

    public function __toString()
    {
        return $this->getItem()." (Cantidad: {$this->getAmount()} - Precio: {$this->getPrice()})";
    }

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * Get Total Price value witout discount and without tax (euros) 
     * 
     * @return decimal
     */
    public function getPriceTotal()
    {
        return round($this->amount * $this->price, 2);
    }
    
    /**
     * Get Discount value (euros)
     * 
     * @return decimal
     */
    public function getDiscountValue()
    {
        return round($this->getPriceTotal() * $this->discount, 2);
    }
    
    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return decimal
     */
    public function getTaxValue()
    {
        return $this->getBase() * $this->tax;
    }
    
    /**
     * Get Total with discount and without tax (euros)
     * 
     * @return decimal
     */
    public function getBase()
    {
        return round($this->getPriceTotal() - $this->getDiscountValue(), 2);
    }

    /**
     * Get Total with discount and with tax (euros)
     * 
     * @return decimal
     */
    public function getTotal()
    {
        return round($this->getBase() + $this->getTaxValue(), 2);
    }
}