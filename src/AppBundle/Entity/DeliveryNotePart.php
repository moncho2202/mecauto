<?php

namespace AppBundle\Entity;

/**
 * DeliveryNotePart
 */
class DeliveryNotePart
{
    /**
     * @var integer
     */
    private $id;
    
    /**
     * @var string
     */
    private $discount;

    /**
     * @var string
     */
    private $tax;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var string
     */
    private $price;

    /**
     * @var \AppBundle\Entity\DeliveryNote
     */
    private $deliveryNote;

    /**
     * @var \AppBundle\Entity\Part
     */
    private $part;

    /**
     * @var \AppBundle\Entity\Input
     */
    private $input;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return DeliveryNotePart
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Get discount next
     *
     * @return null
     */
    public function getDiscountNext()
    {
        return null;
    }

    /**
     * Set discount next
     * Aplicar descuentos sucesivos
     *
     * @param string $discount
     * @return \AppBundle\Entity\Item
     */
    public function setDiscountNext($discount)
    {
        // Calcular suma de descuentos
        $this->discount = 1-(1-$this->discount)*(1-$discount);

        return $this;
    }

    /**
     * Get price neto
     *
     * @return null
     */
    public function getPriceNeto()
    {
        return null;
    }

    /**
     * Set price neto
     * Aplicar descuentos netos
     *
     * @param string $price
     * @return \AppBundle\Entity\Item
     */
    public function setPriceNeto($price)
    {
        // Calcular suma de descuentos
        $this->discount = 1 - ($price / $this->price);

        return $this;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return DeliveryNotePart
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return DeliveryNotePart
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return DeliveryNotePart
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set deliveryNote
     *
     * @param \AppBundle\Entity\DeliveryNote $deliveryNote
     *
     * @return DeliveryNotePart
     */
    public function setDeliveryNote(\AppBundle\Entity\DeliveryNote $deliveryNote)
    {
        $this->deliveryNote = $deliveryNote;

        return $this;
    }

    /**
     * Get deliveryNote
     *
     * @return \AppBundle\Entity\DeliveryNote
     */
    public function getDeliveryNote()
    {
        return $this->deliveryNote;
    }

    /**
     * Set part
     *
     * @param \AppBundle\Entity\Part $part
     *
     * @return DeliveryNotePart
     */
    public function setPart(\AppBundle\Entity\Part $part)
    {
        $this->part = $part;

        return $this;
    }

    /**
     * Get part
     *
     * @return \AppBundle\Entity\Part
     */
    public function getPart()
    {
        return $this->part;
    }

    /**
     * Set input
     *
     * @param \AppBundle\Entity\Input $input
     *
     * @return DeliveryNotePart
     */
    public function setInput(\AppBundle\Entity\Input $input = null)
    {
        $this->input = $input;

        return $this;
    }

    /**
     * Get input
     *
     * @return \AppBundle\Entity\Input
     */
    public function getInput()
    {
        return $this->input;
    }

    public function __toString()
    {
        return (string) $this->getPart();
    }

    /**
     * Get Total Price value witout discount and without tax (euros) 
     * 
     * @return decimal
     */
    public function getPriceTotal()
    {
        return $this->amount * $this->price;
    }

    /**
     * Get Discount value (euros)
     * 
     * @return decimal
     */
    public function getDiscountValue()
    {
        return round($this->getPriceTotal() * $this->discount, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return decimal
     */
    public function getTaxValue()
    {
        return round(($this->getPriceTotal() - $this->getDiscountValue()) * $this->tax, 2);
    }

    /**
     * Get Total with discount and without tax (euros)
     * 
     * @return decimal
     */
    public function getBase()
    {
        return round($this->getPriceTotal() - $this->getDiscountValue(), 2);
    }

    /**
     * Get Total with discount and with tax (euros)
     * 
     * @return decimal
     */
    public function getTotal()
    {
        return round($this->getBase() + $this->getTaxValue(), 2);
    }
}
