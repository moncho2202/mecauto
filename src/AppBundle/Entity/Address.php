<?php

namespace AppBundle\Entity;

/**
 * Address
 */
class Address
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $default = 0;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \AppBundle\Entity\AddressData
     */
    private $addr;

    /**
     * @var \AppBundle\Entity\Customer
     */
    private $customer;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set default
     *
     * @param boolean $default
     *
     * @return Address
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return boolean
     */
    public function getDefault()
    {
        return $this->default;
    }
    
    /**
     * Is default
     *
     * @return boolean
     */
    public function isDefault()
    {
        return (boolean) $this->default;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Address
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Address
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set addr
     *
     * @param \AppBundle\Entity\AddressData $addr
     *
     * @return Address
     */
    public function setAddr(\AppBundle\Entity\AddressData $addr)
    {
        $this->addr = $addr;

        return $this;
    }

    /**
     * Get addr
     *
     * @return \AppBundle\Entity\AddressData
     */
    public function getAddr()
    {
        return $this->addr;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\Customer $customer
     *
     * @return Address
     */
    public function setCustomer(\AppBundle\Entity\Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
    
    public function __toString() {
        return trim($this->getStreet()." ".$this->getCity()." ".$this->getState()." ".$this->getCode());
    }
    
    // Get Addr fields
    public function getStreet()
    {
        return $this->getAddr()->getStreet();
    }
    public function getCity()
    {
        return $this->getAddr()->getCity();
    }
    public function getState()
    {
        return $this->getAddr()->getState();
    }
    public function getCode()
    {
        return $this->getAddr()->getCode();
    }
}
