<?php

namespace AppBundle\Entity;

/**
 * CustomerData
 */
class VehicleData
{
    /**
     * @var string
     */
    private $registration;

    /**
     * @var string
     */
    private $trademark;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $vin;

    /**
     * @var string
     */
    private $engine;

    /**
     * @var string
     */
    private $colour;

    /**
     * @var string
     */
    private $colour_code;

    public function __construct(Vehicle $vehicle = null)
    {
        if ($vehicle){
            $this->setData($vehicle);
        }
    }

    public function setData(Vehicle $vehicle)
    {
        $this->setColour($vehicle->getColour());
        $this->setColourCode($vehicle->getColourCode());
        $this->setEngine($vehicle->getEngine());
        $this->setModel($vehicle->getModel());
        $this->setRegistration($vehicle->getRegistration());
        $this->setTrademark($vehicle->getTrademark());
        $this->setVin($vehicle->getVin());
    }

    /**
     * Set registration
     *
     * @param string $registration
     *
     * @return Vehicle
     */
    public function setRegistration($registration)
    {
        $this->registration = strtoupper($registration);

        return $this;
    }

    /**
     * Get registration
     *
     * @return string
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * Set trademark
     *
     * @param string $trademark
     *
     * @return Vehicle
     */
    public function setTrademark($trademark)
    {
        $this->trademark = $trademark;

        return $this;
    }

    /**
     * Get trademark
     *
     * @return string
     */
    public function getTrademark()
    {
        return $this->trademark;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Vehicle
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set vin
     *
     * @param string $vin
     *
     * @return Vehicle
     */
    public function setVin($vin)
    {
        $this->vin = strtoupper($vin);

        return $this;
    }

    /**
     * Get vin
     *
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * Set engine
     *
     * @param string $engine
     *
     * @return Vehicle
     */
    public function setEngine($engine)
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * Get engine
     *
     * @return string
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * Set colour
     *
     * @param string $colour
     *
     * @return Vehicle
     */
    public function setColour($colour)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Get colour
     *
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * Set colourCode
     *
     * @param string $colourCode
     *
     * @return Vehicle
     */
    public function setColourCode($colourCode)
    {
        $this->colour_code = $colourCode;

        return $this;
    }

    /**
     * Get colourCode
     *
     * @return string
     */
    public function getColourCode()
    {
        return $this->colour_code;
    }

    public function getTrademarkModel()
    {
        return trim($this->getTrademark()." ".$this->getModel());
    }

    public function __toString()
    {
        return trim($this->getRegistration()." ".$this->getTrademarkModel());
    }
}
