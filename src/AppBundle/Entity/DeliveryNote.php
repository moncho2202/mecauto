<?php

namespace AppBundle\Entity;

/**
 * DeliveryNote
 */
class DeliveryNote
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $parts;

    /**
     * @var \AppBundle\Entity\Provider
     */
    private $provider;

    /**
     * @var \AppBundle\Entity\ProviderInvoice
     */
    private $invoice;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return DeliveryNote
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DeliveryNote
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return DeliveryNote
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return DeliveryNote
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add part
     *
     * @param \AppBundle\Entity\DeliveryNotePart $part
     *
     * @return DeliveryNote
     */
    public function addPart(\AppBundle\Entity\DeliveryNotePart $part)
    {
        $this->parts[] = $part;
        $part->setDeliveryNote($this);

        return $this;
    }

    /**
     * Remove part
     *
     * @param \AppBundle\Entity\DeliveryNotePart $part
     */
    public function removePart(\AppBundle\Entity\DeliveryNotePart $part)
    {
        $this->parts->removeElement($part);
    }

    /**
     * Get parts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * Set provider
     *
     * @param \AppBundle\Entity\Provider $provider
     *
     * @return DeliveryNote
     */
    public function setProvider(\AppBundle\Entity\Provider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AppBundle\Entity\Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\ProviderInvoice $invoice
     *
     * @return DeliveryNote
     */
    public function setInvoice(\AppBundle\Entity\ProviderInvoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\ProviderInvoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function __toString()
    {
        return trim($this->getProvider()." ".$this->getNumber());
    }

    /**
     * Get Total with discount and without tax (euros)
     * 
     * @return decimal
     */
    public function getBase()
    {
        $base = 0;
        foreach ($this->getParts() as $part){
            $base += $part->getBase();
        }

        return round($base, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return decimal
     */
    public function getTax()
    {
        $tax = 0;
        foreach ($this->getParts() as $part){
            $tax += $part->getTaxValue();
        }

        return round($tax, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return decimal
     */
    public function getTotal()
    {
        return round($this->getBase()+$this->getTax(), 2);
    }

    public function getPaid()
    {
        if (empty($this->getInvoice())){
            return false;
        }
        
        return $this->getInvoice()->getPaid();
    }
}
