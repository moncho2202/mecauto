<?php

namespace AppBundle\Entity;

/**
 * InvoiceRefund
 */
class InvoiceRefund extends Bill
{
    private $invoice;

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return InvoiceRefund
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice)
    {
        $this->invoice = $invoice;
        $this->setCompany($invoice->getCompany());
        $this->setCustomer($invoice->getCustomer());
        $this->setAddress($invoice->getAddress());

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    public function copyInvoice(Invoice $invoice)
    {
        $this->setInvoice($invoice);
        foreach ($invoice->getItems() as $invoiceItem){
            $item = new InvoiceItem();
            $item->setAmount($invoiceItem->getAmount());
            $item->setDiscount($invoiceItem->getDiscount());
            $item->setItem($invoiceItem->getItem());
            $item->setPrice(-$invoiceItem->getPrice());
            $item->setTax($invoiceItem->getTax());
            $this->addItem($item);
        }
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return Bill
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        $prefix = $this->getCompany()->getPrefixInvoiceRefund();
        $this->setNumber($prefix.date('y').str_pad($sequence, 5, "00000", STR_PAD_LEFT));

        return $this;
    }
}
