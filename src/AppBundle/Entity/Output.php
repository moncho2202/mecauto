<?php

namespace AppBundle\Entity;

/**
 * Output
 */
class Output extends Entry
{
    /**
     * @var \AppBundle\Entity\InvoiceItem
     */
    private $invoiceItem;


    /**
     * Set invoiceItem
     *
     * @param \AppBundle\Entity\InvoiceItem $invoiceItem
     *
     * @return Output
     */
    public function setInvoiceItem(\AppBundle\Entity\InvoiceItem $invoiceItem)
    {
        $this->invoiceItem = $invoiceItem;

        return $this;
    }

    /**
     * Get invoiceItem
     *
     * @return \AppBundle\Entity\InvoiceItem
     */
    public function getInvoiceItem()
    {
        return $this->invoiceItem;
    }
}
