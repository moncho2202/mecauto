<?php

namespace AppBundle\Entity;

/**
 * InvoiceReceipt
 */
class InvoiceReceipt
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $amount;
    
    /**
     * @var string
     */
    private $paid = 0;
    
    /**
     * @var bool
     */
    private $is_paid;

    /**
     * @var CustomerData
     */
    private $customer;

    /**
     * @var AddressData
     */
    private $address;

    /**
     * @var VehicleData
     */
    private $vehicle;

   /**
     * @var \AppBundle\Entity\Invoice
     */
    private $invoice;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InvoiceReceipt
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return InvoiceReceipt
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set paid
     *
     * @param string $paid
     *
     * @return InvoiceReceipt
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return string
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set is paid
     *
     * @param boolean $paid
     *
     * @return InvoiceReceipt
     */
    public function setIsPaid($paid)
    {
        if ($paid){
            $this->setPaid($this->getAmount());
        }

        return $this;
    }

    /**
     * Get is paid
     *
     * @return bool
     */
    public function getIsPaid()
    {
        return $this->getPaid() >= $this->getAmount();
    }
    public function isPaid()
    {
        return $this->getIsPaid();
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\CustomerData $customerData
     *
     * @return InvoiceReceipt
     */
    public function setCustomer(\AppBundle\Entity\CustomerData $customerData)
    {
        $this->customer = $customerData;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\CustomerData
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\AddressData $addressData
     *
     * @return InvoiceReceipt
     */
    public function setAddress(\AppBundle\Entity\AddressData $addressData)
    {
        $this->address = $addressData;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\AddressData
     */
    public function getAddress()
    {
        return $this->address;
    }
  
    /**
     * Set vehicle
     *
     * @param \AppBundle\Entity\VehicleData $vehicleData
     *
     * @return InvoiceReceipt
     */
    public function setVehicle(\AppBundle\Entity\VehicleData $vehicleData)
    {
        $this->vehicle = $vehicleData;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return \AppBundle\Entity\VehicleData
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return InvoiceReceipt
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Alias get customer identifier for sonata (embedded not working)
     * 
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getCustomer()->getIdentifier();
    }

    /**
     * Alias get customer fullname for sonata (embedded not working)
     * 
     * @return string
     */
    public function getFullname()
    {
        return $this->getCustomer()->getFullname();
    }

   /**
     * Alias get vehicle registration for sonata (embedded not working)
     * 
     * @return string
     */
    public function getRegistration()
    {
        return $this->getVehicle()->getRegistration();
    }
}

