<?php

namespace AppBundle\Entity;

/**
 * Numbers
 */
class Numbers
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $numberOe;

    /**
     * @var int
     */
    private $numberOr;

    /**
     * @var int
     */
    private $numberBudget;

    /**
     * @var int
     */
    private $numberInvoiceRefund;

    /**
     * @var int
     */
    private $numberInvoice;

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;
    
    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numberOe
     *
     * @param integer $numberOe
     *
     * @return Numbers
     */
    public function setNumberOe($numberOe)
    {
        $this->numberOe = $numberOe;

        return $this;
    }

    /**
     * Get numberOe
     *
     * @return int
     */
    public function getNumberOe()
    {
        return $this->numberOe;
    }

    /**
     * Set numberOr
     *
     * @param integer $numberOr
     *
     * @return Numbers
     */
    public function setNumberOr($numberOr)
    {
        $this->numberOr = $numberOr;

        return $this;
    }

    /**
     * Get numberOr
     *
     * @return int
     */
    public function getNumberOr()
    {
        return $this->numberOr;
    }

    /**
     * Set numberBudget
     *
     * @param integer $numberBudget
     *
     * @return Numbers
     */
    public function setNumberBudget($numberBudget)
    {
        $this->numberBudget = $numberBudget;

        return $this;
    }

    /**
     * Get numberBudget
     *
     * @return int
     */
    public function getNumberBudget()
    {
        return $this->numberBudget;
    }

    /**
     * Set numberInvoice
     *
     * @param integer $numberInvoice
     *
     * @return Numbers
     */
    public function setNumberInvoice($numberInvoice)
    {
        $this->numberInvoice = $numberInvoice;

        return $this;
    }

    /**
     * Get numberInvoice
     *
     * @return int
     */
    public function getNumberInvoice()
    {
        return $this->numberInvoice;
    }

    /**
     * Set numberInvoiceRefund
     *
     * @param integer $numberInvoiceRefund
     *
     * @return Numbers
     */
    public function setNumberInvoiceRefund($numberInvoiceRefund)
    {
        $this->numberInvoiceRefund = $numberInvoiceRefund;

        return $this;
    }

    /**
     * Get numberInvoiceRefund
     *
     * @return int
     */
    public function getNumberInvoiceRefund()
    {
        return $this->numberInvoiceRefund;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return Numbers
     */
    public function setCompany(\AppBundle\Entity\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Numbers
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Company
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
}

