<?php /** @noinspection PhpUnused */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Invoice
 */
class Invoice extends Bill
{
    /**
     * @var string
     */
    protected $excess;

    /**
     * @var Vehicle
     */
    protected $vehicle;

    /**
     * @var VehicleData
     */
    protected $vehicle_data;

    /**
     * @var OrderEntry
     */
    protected $oe;

    /**
     * @var Budget|null
     */
    protected $budget;

    /**
     * @var InvoiceReceipt
     */
    protected $receipt;

    /**
     * @var InvoiceRefund
     */
    protected $refunds;

    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * @var Invoice
     */
    protected $child;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->refunds = new ArrayCollection();
        
        $this->excess = 0;
    }

    /**
     * Set excess
     *
     * @param string $excess
     *
     * @return Invoice
     */
    public function setExcess($excess)
    {
        $this->excess = $excess;

        return $this;
    }

    /**
     * Get excess
     *
     * @return string
     */
    public function getExcess()
    {
        return $this->excess;
    }

    /**
     * Set vehicle
     *
     * @param Vehicle $vehicle
     *
     * @return Invoice
     */
    public function setVehicle(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
        
        return $this;
    }

    /**
     * Get vehicle
     *
     * @return Vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set vehicle
     *
     * @param VehicleData $vehicleData
     *
     * @return Invoice
     */
    public function setVehicleData(VehicleData $vehicleData)
    {
        $this->vehicle_data = $vehicleData;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return VehicleData
     */
    public function getVehicleData()
    {
        return $this->vehicle_data;
    }

    /**
     * Add refund
     *
     * @param InvoiceRefund $refund
     *
     * @return Invoice
     */
    public function addRefund(InvoiceRefund $refund)
    {
        $this->refunds[] = $refund;

        return $this;
    }

    /**
     * Remove refund
     *
     * @param InvoiceRefund $refund
     */
    public function removeRefund(InvoiceRefund $refund)
    {
        $this->refunds->removeElement($refund);
    }

    /**
     * Get refunds
     *
     * @return Collection
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * Set invoice
     *
     * @param Invoice $invoice
     *
     * @return Invoice
     */
    public function setInvoice(Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set child
     *
     * @param Invoice $invoice
     *
     * @return Invoice
     */
    public function setChild(Invoice $invoice = null)
    {
        $this->child = $invoice;

        return $this;
    }

    /**
     * Get child
     *
     * @return Invoice
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set oe
     *
     * @param OrderEntry $oe
     *
     * @return Invoice
     */
    public function setOe(OrderEntry $oe)
    {
        $this->oe = $oe;
        $this->setCompany($oe->getCompany());

        return $this;
    }

    /**
     * Get oe
     *
     * @return OrderEntry
     */
    public function getOe()
    {
        return $this->oe;
    }

    /**
     * @return Budget|null
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param Budget|null $budget
     * @return Invoice
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getKm()
    {
        return $this->oe ? $this->oe->getKm() : null;
    }

    /**
     * Set receipt
     *
     * @param InvoiceReceipt $receipt
     *
     * @return Invoice
     */
    public function setReceipt(InvoiceReceipt $receipt)
    {
        $this->receipt = $receipt;

        return $this;
    }

    /**
     * Get receipt
     *
     * @return InvoiceReceipt
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    public function prePersist()
    {
        parent::prePersist();
        $this->total = $this->getTotal();
    }

    public function preUpdate()
    {
        parent::preUpdate();
        $this->total = $this->getTotal();
    }

    public function copyBudget(Budget $budget)
    {
        $this->setOe($budget->getOe());
        $this->setBudget($budget);
        foreach ($budget->getItems() as $budgetItem){
            $item = new InvoiceItem();
            $item->setAmount($budgetItem->getAmount());
            $item->setDescription($budgetItem->getDescription());
            $item->setDiscount($budgetItem->getDiscount());
            $item->setItem($budgetItem->getItem());
            $item->setPrice($budgetItem->getPrice());
            $item->setTax($budgetItem->getTax());
            $this->addItem($item);
        }
    }

   /**
     * Alias get vehicle registration for sonata (embedded not working)
     * 
     * @return string
     */
    public function getRegistration()
    {
        return $this->getVehicleData()->getRegistration();
    }

    /**
     * Get Total value of price with discount and tax applied (euros) 
     * 
     * @return float
     */
    public function getTotal()
    {
        $total = parent::getTotal();
        if ($this->getExcess() > 0){
            $total -= $this->getExcess();
        }
        return round($total, 2);
    }

    /**
     * Get Totals of budget associated to invoice
     *
     * @return float
     */
    public function getTotalBudget()
    {
        return $this->budget ? round($this->budget->getTotal(),2) : 0;
    }

    /**
     * Get Totals of budgets associated to OE
     * 
     * @return float
     */
    public function getTotalBudgets()
    {
        $total = 0;
        foreach ($this->oe->getBudgets() as $budget){
            $total += round($budget->getTotal(),2);
        }
        
        return round($total, 2);
    }
}
