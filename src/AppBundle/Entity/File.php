<?php

namespace AppBundle\Entity;

/**
 * File
 */
class File
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * Binary data
     *
     * @var string
     */
    protected $file;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $mime;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return File
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Guardar fichero en base de datos y fijar el filename y mime
     */
    public function upload()
    {
        if ($this->filename instanceof \Symfony\Component\HttpFoundation\File\UploadedFile){
            $this->setMime($this->filename->getMimeType());
            $this->setFile(file_get_contents($this->filename->getRealPath()));
            $this->filename = $this->filename->getClientOriginalName();
        }
    }

    /**
     * Get file as binary
     *
     * @return string
     */
    public function getFileAsBin()
    {
        if (!$this->file){
            return;
        }

        return stream_get_contents($this->file);
    }

    /**
     * Get file as base64
     *
     * @return string
     */
    public function getFileAsBase64()
    {
        if (!$this->file){
            return;
        }

        return base64_encode($this->getFileAsBin());
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set mime
     *
     * @param string $mime
     *
     * @return File
     */
    public function setMime($mime)
    {
        $this->mime = $mime;

        return $this;
    }

    /**
     * Get mime
     *
     * @return string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return File
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Item
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    public function preUpdate()
    {
    }

    public function __toString()
    {
        return $this->getDescription();
    }
}
