<?php

namespace AppBundle\Entity;

/**
 * VehicleFile
 */
class VehicleFile extends File
{

    /**
     * @var \AppBundle\Entity\Vehicle
     */
    private $vehicle;

    /**
     * Set vehicle
     *
     * @param \AppBundle\Entity\Vehicle $vehicle
     *
     * @return VehicleFile
     */
    public function setVehicle(\AppBundle\Entity\Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return \AppBundle\Entity\Vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }
}
