<?php

namespace AppBundle\Entity;

/**
 * Company
 */
class Company
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $movil;

    /**
     * @var string
     */
    private $registry;

    /**
     * @var string
     */
    private $registered;

    /**
     * @var string
     */
    private $default_tax;

    /**
     * @var string
     */
    private $default_price_by_hour;

    /**
     * @var string
     */
    private $prefix_oe;

    /**
     * @var string
     */
    private $prefix_or;

    /**
     * @var string
     */
    private $prefix_budget;

    /**
     * @var string
     */
    private $prefix_invoice;

    /**
     * @var string
     */
    private $prefix_invoice_refund;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \AppBundle\Entity\AddressData
     */
    private $addr;

    /**
     * @var \AppBundle\Entity\Numbers
     */
    private $numbers;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Company
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     *
     * @return Company
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set movil
     *
     * @param string $movil
     *
     * @return Company
     */
    public function setMovil($movil)
    {
        $this->movil = $movil;

        return $this;
    }

    /**
     * Get movil
     *
     * @return string
     */
    public function getMovil()
    {
        return $this->movil;
    }

    /**
     * Set registry
     *
     * @param string $registry
     *
     * @return Company
     */
    public function setRegistry($registry)
    {
        $this->registry = $registry;

        return $this;
    }

    /**
     * Get registry
     *
     * @return string
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * Set registered
     *
     * @param string $registered
     *
     * @return Company
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;

        return $this;
    }

    /**
     * Get registered
     *
     * @return string
     */
    public function getRegistered()
    {
        return $this->registered;
    }

    /**
     * Set defaultTax
     *
     * @param string $defaultTax
     *
     * @return Company
     */
    public function setDefaultTax($defaultTax)
    {
        $this->default_tax = $defaultTax;

        return $this;
    }

    /**
     * Get defaultTax
     *
     * @return string
     */
    public function getDefaultTax()
    {
        return $this->default_tax;
    }

    /**
     * Set defaultPriceByHour
     *
     * @param string $defaultPriceByHour
     *
     * @return Company
     */
    public function setDefaultPriceByHour($defaultPriceByHour)
    {
        $this->default_price_by_hour = $defaultPriceByHour;

        return $this;
    }

    /**
     * Get defaultPriceByHour
     *
     * @return string
     */
    public function getDefaultPriceByHour()
    {
        return $this->default_price_by_hour;
    }

    /**
     * Set prefix_oe
     *
     * @param string $prefixOe
     *
     * @return Company
     */
    public function setPrefixOe($prefixOe)
    {
        $this->prefix_oe = $prefixOe;

        return $this;
    }

    /**
     * Get prefix_oe
     *
     * @return string
     */
    public function getPrefixOe()
    {
        return $this->prefix_oe;
    }

    /**
     * Set prefix_or
     *
     * @param string $prefixOr
     *
     * @return Company
     */
    public function setPrefixOr($prefixOr)
    {
        $this->prefix_or = $prefixOr;

        return $this;
    }

    /**
     * Get prefix_or
     *
     * @return string
     */
    public function getPrefixOr()
    {
        return $this->prefix_or;
    }

    /**
     * Set prefix_budget
     *
     * @param string $prefixBudget
     *
     * @return Company
     */
    public function setPrefixBudget($prefixBudget)
    {
        $this->prefix_budget = $prefixBudget;

        return $this;
    }

    /**
     * Get prefix_budget
     *
     * @return string
     */
    public function getPrefixBudget()
    {
        return $this->prefix_budget;
    }

    /**
     * Set prefix_invoice
     *
     * @param string $prefixInvoice
     *
     * @return Company
     */
    public function setPrefixInvoice($prefixInvoice)
    {
        $this->prefix_invoice = $prefixInvoice;

        return $this;
    }

    /**
     * Get prefix_invoice
     *
     * @return string
     */
    public function getPrefixInvoice()
    {
        return $this->prefix_invoice;
    }

    /**
     * Get prefix_invoice_refund
     *
     * @return string
     */
    public function getPrefixInvoiceRefund()
    {
        return $this->prefix_invoice_refund;
    }

    /**
     * Set prefix_invoice_refund
     *
     * @param string $prefix_invoice_refund
     *
     * @return Company
     */
    public function setPrefixInvoiceRefund($prefix_invoice_refund)
    {
        $this->prefix_invoice_refund = $prefix_invoice_refund;

        return $this;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Company
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Company
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set addr
     *
     * @param \AppBundle\Entity\AddressData $addr
     *
     * @return Company
     */
    public function setAddr(\AppBundle\Entity\AddressData $addr)
    {
        $this->addr = $addr;

        return $this;
    }

    /**
     * Get addr
     *
     * @return \AppBundle\Entity\AddressData
     */
    public function getAddr()
    {
        return $this->addr;
    }

    /**
     * Set numbers
     *
     * @param \AppBundle\Entity\Numbers $numbers
     *
     * @return Company
     */
    public function setNumbers(\AppBundle\Entity\Numbers $numbers)
    {
        $this->numbers = $numbers;
        $numbers->setCompany($this);

        return $this;
    }

    /**
     * Get numbers
     *
     * @return \AppBundle\Entity\Numbers
     */
    public function getNumbers()
    {
        return $this->numbers;
    }

    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
    
    public function __toString() {
        return trim($this->getName()." (".$this->getAlias().")");
    }

    // Get Addr fields
    public function getStreet()
    {
        return $this->getAddr()->getStreet();
    }
    public function getCity()
    {
        return $this->getAddr()->getCity();
    }
    public function getState()
    {
        return $this->getAddr()->getState();
    }
    public function getCode()
    {
        return $this->getAddr()->getCode();
    }
}
