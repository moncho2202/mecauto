<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Budget
 */
class Budget
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $sequence;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var DateTime
     */
    protected $created_at;

    /**
     * @var DateTime
     */
    protected $updated_at;

    /**
     * @var Collection
     */
    protected $items;

    /**
     * @var OrderEntry
     */
    protected $oe;

    /**
     * @var Collection<Invoice>
     */
    protected $invoices;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->date = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return Budget
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        $prefix = $this->oe->getCompany()->getPrefixBudget();
        $this->setNumber($prefix.date('y').str_pad($sequence, 5, "00000", STR_PAD_LEFT));
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSecuence()
    {
        return $this->sequence;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Budget
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set date
     *
     * @param DateTime $date
     *
     * @return Budget
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Budget
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Budget
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add item
     *
     * @param BudgetItem $item
     *
     * @return Budget
     */
    public function addItem(BudgetItem $item)
    {
        $this->items[] = $item;
        $item->setBudget($this);
        
        return $this;
    }

    /**
     * Remove item
     *
     * @param BudgetItem $item
     */
    public function removeItem(BudgetItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set oe
     *
     * @param OrderEntry $oe
     *
     * @return Budget
     */
    public function setOe(OrderEntry $oe)
    {
        $this->oe = $oe;

        return $this;
    }

    /**
     * Get oe
     *
     * @return OrderEntry
     */
    public function getOe()
    {
        return $this->oe;
    }

    /**
     * @return Collection<Invoice>
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * @return boolean
     */
    public function hasInvoice()
    {
        return (boolean) $this->invoices->count();
    }

    public function prePersist()
    {
        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
    }

    public function preUpdate()
    {
        $this->setUpdatedAt(new DateTime());
    }

    public function __toString()
    {
        return $this->getNumber();
    }

    /**
     * Get Total with discount and without tax (euros) only parts
     * 
     * @return float
     */
    public function getBaseParts()
    {
        $base = 0;
        foreach ($this->getItems() as $item){
            if ($item->getItem() instanceof Part){
                $base += $item->getBase();
            }
        }

        return round($base, 2);
    }

    /**
     * Get Total with discount and without tax (euros) only labors
     * 
     * @return float
     */
    public function getBaseLabors()
    {
        $base = 0;
        foreach ($this->getItems() as $item){
            if ($item->getItem() instanceof Labor){
                $base += $item->getBase();
            }
        }

        return round($base, 2);
    }
    
    /**
     * Get Total with discount and without tax (euros)
     * 
     * @return float
     */
    public function getBase()
    {
        $base = 0;
        foreach ($this->getItems() as $item){
            $base += $item->getBase();
        }

        return round($base, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return float
     */
    public function getTax()
    {
        $tax = 0;
        foreach ($this->getItems() as $item){
            $tax += $item->getTaxValue();
        }

        return round($tax, 2);
    }

    /**
     * Get Tax value of price with discount applied (euros) 
     * 
     * @return float
     */
    public function getTotal()
    {
        return round($this->getBase()+$this->getTax(), 2);
    }

    /**
     * Get Totals of invoices associated to OE
     * 
     * @return string
     */
    public function getTotalInvoices()
    {
        $total = [];
        foreach ($this->getInvoices() as $invoice){
            $total[] = number_format($invoice->getTotal(),2,",",".")." €";
        }
        
        return implode($total, ", ");
    }
}
