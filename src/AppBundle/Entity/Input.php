<?php

namespace AppBundle\Entity;

/**
 * Input
 */
class Input extends Entry
{
    /**
     * @var \AppBundle\Entity\DeliveryNotePart
     */
    private $deliveryNotePart;


    /**
     * Set deliveryNotePart
     *
     * @param \AppBundle\Entity\DeliveryNotePart $deliveryNotePart
     *
     * @return Input
     */
    public function setDeliveryNotePart(\AppBundle\Entity\DeliveryNotePart $deliveryNotePart)
    {
        $this->deliveryNotePart = $deliveryNotePart;

        return $this;
    }

    /**
     * Get deliveryNotePart
     *
     * @return \AppBundle\Entity\DeliveryNotePart
     */
    public function getDeliveryNotePart()
    {
        return $this->deliveryNotePart;
    }
}
