<?php

namespace AppBundle\Entity;

use AppBundle\Validator\Constraints\NifValidator;

/**
 * Vehicle
 */
class Vehicle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $registration;

    /**
     * @var string
     */
    private $trademark;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $vin;

    /**
     * @var string
     */
    private $engine;

    /**
     * @var string
     */
    private $colour;

    /**
     * @var string
     */
    private $colour_code;

    /**
     * @var string
     */
    private $note;

    /**
     * @var \DateTime
     */
    private $date_making;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \AppBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $files;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registration
     *
     * @param string $registration
     *
     * @return Vehicle
     */
    public function setRegistration($registration)
    {
        $this->registration = strtoupper($registration);

        return $this;
    }

    /**
     * Get registration
     *
     * @return string
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * Set trademark
     *
     * @param string $trademark
     *
     * @return Vehicle
     */
    public function setTrademark($trademark)
    {
        $this->trademark = $trademark;

        return $this;
    }

    /**
     * Get trademark
     *
     * @return string
     */
    public function getTrademark()
    {
        return $this->trademark;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Vehicle
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set vin
     *
     * @param string $vin
     *
     * @return Vehicle
     */
    public function setVin($vin)
    {
        $this->vin = strtoupper($vin);

        return $this;
    }

    /**
     * Get vin
     *
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * Set engine
     *
     * @param string $engine
     *
     * @return Vehicle
     */
    public function setEngine($engine)
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * Get engine
     *
     * @return string
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * Set colour
     *
     * @param string $colour
     *
     * @return Vehicle
     */
    public function setColour($colour)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Get colour
     *
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * Set colourCode
     *
     * @param string $colourCode
     *
     * @return Vehicle
     */
    public function setColourCode($colourCode)
    {
        $this->colour_code = $colourCode;

        return $this;
    }

    /**
     * Get colourCode
     *
     * @return string
     */
    public function getColourCode()
    {
        return $this->colour_code;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Vehicle
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set dateMaking
     *
     * @param \DateTime $dateMaking
     *
     * @return Vehicle
     */
    public function setDateMaking($dateMaking)
    {
        $this->date_making = $dateMaking;

        return $this;
    }

    /**
     * Get dateMaking
     *
     * @return \DateTime
     */
    public function getDateMaking()
    {
        return $this->date_making;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Vehicle
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Vehicle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\Customer $customer
     *
     * @return Vehicle
     */
    public function setCustomer(\AppBundle\Entity\Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add file
     *
     * @param \AppBundle\Entity\VehicleFile $file
     *
     * @return Vehicle
     */
    public function addFile(\AppBundle\Entity\VehicleFile $file)
    {
        $file->setVehicle($this);
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \AppBundle\Entity\VehicleFile $file
     */
    public function removeFile(\AppBundle\Entity\VehicleFile $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    private function clearForcedRegistration()
    {
        if (strpos($this->registration, NifValidator::FORCE_NIF_KEY) === 0) {
            $this->registration = substr($this->registration, 3);
        }
    }

    public function prePersist()
    {
        $this->clearForcedRegistration();
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    public function preUpdate()
    {
        $this->clearForcedRegistration();
        $this->setUpdatedAt(new \DateTime());
    }
    
    public function __toString()
    {
        return trim($this->getRegistration()." ".$this->getTrademarkModel());
    }
    
    public function getTrademarkModel()
    {
        return trim($this->getTrademark()." ".$this->getModel());
    }
    
    public function getCustomerVehicle()
    {
        return $this->getCustomer()->getIdentifier()." ".
                $this->getCustomer()->getFullname()." ".
                $this->getRegistration()." ".$this->getTrademarkModel();
    }
}
