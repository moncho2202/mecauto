<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Labor
 */
class Labor extends Item
{

    /**
     * @var \AppBundle\Entity\LaborType
     */
    private $type;

    /**
     * @var string
     */
    private $unit;

    /**
     * No save in database, only for calculate unit
     * 
     * @var string
     */
    private $price;

    /**
     * No save in database, only for calculate unit
     * 
     * @var string
     */
    private $hour;

    /**
     * Set type
     *
     * @param \AppBundle\Entity\LaborType $type
     *
     * @return Labor
     */
    public function setType(\AppBundle\Entity\LaborType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\LaborType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return Labor
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }
    
    /**
     * Set price
     *
     * @param string $price
     *
     * @return Labor
     */
    public function setPrice($price)
    {
        if ($price){
            $this->setValue($price);
            $this->unit = 'EUR';
        }

        $this->price = $price;
        
        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        if ($this->unit == 'EUR'){
            return $this->getValue();
        }else{
            return 0;
        }
    }

    /**
     * Set hour
     *
     * @param string $hour
     *
     * @return Labor
     */
    public function setHour($hour)
    {
        if ($hour){
            $this->setValue($hour);
            $this->unit = 'HOUR';
        }

        $this->hour = $hour;
        
        return $this;
    }

    /**
     * Get hour
     *
     * @return string
     */
    public function getHour()
    {
        if ($this->unit == 'HOUR'){
            return $this->getValue();
        }else{
            return 0;
        }
    }


    public function validate(ExecutionContextInterface $context)
    {
        // check if the name is actually a fake name
        if ($this->hour && $this->price) {
            $context->buildViolation('No puede definir precio y horas a la vez.')
                ->atPath('price')
                ->addViolation();
        }
    }

    /**
     * Get Amount (Amount is default 1)
     * @return int
     */
    public function getAmount()
    {
        if ($this->unit == "HOUR"){
            return $this->getValue();
        }else{
            return 1;
        }
    }
}
