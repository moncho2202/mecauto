<?php

namespace AppBundle\Entity;

/**
 * Part
 */
class Part extends Item
{

    /**
     * @var string
     */
    private $trademark;

    /**
     * @var string
     */
    private $vehicle;

    /**
     * @var integer
     */
    private $stock = 0;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $entries;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set trademark
     *
     * @param string $trademark
     *
     * @return Part
     */
    public function setTrademark($trademark)
    {
        $this->trademark = $trademark;

        return $this;
    }

    /**
     * Get trademark
     *
     * @return string
     */
    public function getTrademark()
    {
        return $this->trademark;
    }

    /**
     * Set vehicle
     *
     * @param string $vehicle
     *
     * @return Part
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return string
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Part
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Add entry
     *
     * @param \AppBundle\Entity\Entry $entry
     *
     * @return Part
     */
    public function addEntry(\AppBundle\Entity\Entry $entry)
    {
        $this->entries[] = $entry;

        return $this;
    }

    /**
     * Remove entry
     *
     * @param \AppBundle\Entity\Entry $entry
     */
    public function removeEntry(\AppBundle\Entity\Entry $entry)
    {
        $this->entries->removeElement($entry);
    }

    /**
     * Get entries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntries()
    {
        return $this->entries;
    }

    public function __toString()
    {
        $string = trim($this->getProvider()." ".$this->getRef()." ".$this->getDescription());
        $string = strlen($string)>70?substr($string,0,70).'...':$string;
        return $string;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Part
     */
    public function setPrice($price)
    {
        $this->setValue($price);

        return $this;
    }

    /**
     * Get price (It is amount field)
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->getValue();
    }

    /**
     * Get Amount (Amount is default 1)
     * @return int
     */
    public function getAmount()
    {
        return 1;
    }
}
