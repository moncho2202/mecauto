<?php

namespace AppBundle\Entity;

/**
 * InvoiceCash
 */
class InvoiceCash extends Bill
{
    /**
     * @var \AppBundle\Entity\InvoiceRefunds
     */
    private $refunds;

    public function __construct() {
        parent::__construct();
        
        $this->refunds = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add refund
     *
     * @param \AppBundle\Entity\InvoiceCashRefund $refund
     *
     * @return InvoiceCash
     */
    public function addRefund(\AppBundle\Entity\InvoiceCashRefund $refund)
    {
        $this->refunds[] = $refund;

        return $this;
    }

    /**
     * Remove refund
     *
     * @param \AppBundle\Entity\InvoiceCashRefund $refund
     */
    public function removeRefund(\AppBundle\Entity\InvoiceCashRefund $refund)
    {
        $this->refunds->removeElement($refund);
    }

    /**
     * Get refunds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRefunds()
    {
        return $this->refunds;
    }
}
