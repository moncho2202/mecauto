<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

use AppBundle\Entity\Bill;
use Application\Sonata\UserBundle\Entity\User;

class InvoiceVoter extends Voter
{
    // these strings are just invented: you can use anything
    const EDIT = 'edit';
    const VIEW = 'view';
    const DELETE = 'delete';
    const SONATA_INVOICE_EDIT = 'ROLE_SONATA_ADMIN_INVOICE_EDIT';
    const SONATA_INVOICE_VIEW = 'ROLE_SONATA_ADMIN_INVOICE_VIEW';
    const SONATA_INVOICE_DELETE = 'ROLE_SONATA_ADMIN_INVOICE_DELETE';
    const SONATA_INVOICE_CASH_EDIT = 'ROLE_SONATA_ADMIN_INVOICE_CASH_EDIT';
    const SONATA_INVOICE_CASH_VIEW = 'ROLE_SONATA_ADMIN_INVOICE_CASH_VIEW';
    const SONATA_INVOICE_CASH_DELETE = 'ROLE_SONATA_ADMIN_INVOICE_CASH_DELETE';
    /**
     *
     * @var AccessDecisionManagerInterface 
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE,
            self::SONATA_INVOICE_VIEW, self::SONATA_INVOICE_EDIT, self::SONATA_INVOICE_DELETE,
            self::SONATA_INVOICE_CASH_EDIT, self::SONATA_INVOICE_CASH_VIEW, self::SONATA_INVOICE_CASH_DELETE,])) {
            return false;
        }

        // only vote on Bill objects inside this voter
        if (!$subject instanceof Bill) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        
        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, ['ROLE_SUPER_ADMIN'])) {
            return true;
        }
        
        if (!$user instanceof User || !$user->getCompany()) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Bill (Invoice, InvoiceCash) object, thanks to supports
        /** @var Bill $invoice */
        $invoice = $subject;

        switch($attribute) {
            case self::VIEW:
            case self::SONATA_INVOICE_VIEW:
            case self::SONATA_INVOICE_CASH_VIEW:
                return $this->canView($invoice, $user);
            case self::EDIT:
            case self::SONATA_INVOICE_EDIT:
            case self::SONATA_INVOICE_CASH_EDIT:
                return $this->canEdit($invoice, $user);
            case self::DELETE:
            case self::SONATA_INVOICE_DELETE:
            case self::SONATA_INVOICE_CASH_DELETE:
                return $this->canDelete($invoice, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }
    
    private function canEdit(Bill $invoice, User $user)
    {        
        // Users only Bill in your company can edit
        return $user->getCompany()->getId() === $invoice->getCompany()->getId();
    }

    private function canView(Bill $invoice, User $user)
    {
        // if they can edit, they can view
        return $this->canEdit($invoice, $user);
    }
    
    private function canDelete(Bill $invoice, User $user)
    {
        // if they can edit, they can delete
        return $this->canEdit($invoice, $user);
    }
}

