<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

use Application\Sonata\UserBundle\Entity\User;

class UserVoter extends Voter
{
    // these strings are just invented: you can use anything
    const EDIT = 'edit';
    const VIEW = 'view';
    const DELETE = 'delete';
    const SONATA_EDIT = 'ROLE_SONATA_USER_ADMIN_USER_EDIT';
    const SONATA_VIEW = 'ROLE_SONATA_USER_ADMIN_USER_VIEW';
    const SONATA_DELETE = 'ROLE_SONATA_USER_ADMIN_USER_DELETE';
    /**
     *
     * @var AccessDecisionManagerInterface 
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE,self::SONATA_VIEW, self::SONATA_EDIT, self::SONATA_DELETE,])) {
            return false;
        }

        // only vote on User objects inside this voter
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        
        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, ['ROLE_SUPER_ADMIN'])) {
            return true;
        }
        
        if (!$user instanceof User || !$user->getCompany()) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a User object, thanks to supports
        /** @var User $usr */
        $usr = $subject;
        if (!$usr->getCompany()){
            // the user must have company; if not, deny access
            return false;
        }

        switch($attribute) {
            case self::VIEW:
            case self::SONATA_VIEW:
                return $this->canView($usr, $user);
            case self::EDIT:
            case self::SONATA_EDIT:
                return $this->canEdit($usr, $user);
            case self::DELETE:
            case self::SONATA_DELETE:
                return $this->canDelete($usr, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }
    
    private function canEdit(User $usr, User $user)
    {        
        // Users only OE in your company can edit
        return $user->getCompany()->getId() === $usr->getCompany()->getId();
    }

    private function canView(User $usr, User $user)
    {
        // if they can edit, they can view
        return $this->canEdit($usr, $user);
    }
    
    private function canDelete(User $usr, User $user)
    {
        // if they can edit, they can delete
        return $this->canEdit($usr, $user);
    }
}

