<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of AddrType
 *
 * @author dfrutos
 */
class AddrType  extends AbstractType
{
    public function __construct()
    {

    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street', null, ['label' => 'Calle, número, piso, puerta ...'])
            ->add('city', null, ['label' => 'Ciudad'])
            ->add('state', null, ['label' => 'Provincia'])
            ->add('code', null, ['label' => 'Código Postal'])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AddressData',
        ));
    }
}
