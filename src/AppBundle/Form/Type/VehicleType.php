<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of AddrType
 *
 * @author dfrutos
 */
class VehicleType  extends AbstractType
{
    public function __construct()
    {

    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('registration', null, ['label' => 'Matricula'])
            ->add('trademark', null, ['label' => 'Marca'])
            ->add('model', null, ['label' => 'Modelo'])
            ->add('vin', null, ['label' => 'Bastidor'])
            ->add('colour', null, ['label' => 'Color'])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\VehicleData',
        ));
    }
}
