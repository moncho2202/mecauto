<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of AddrType
 *
 * @author dfrutos
 */
class CustomerType  extends AbstractType
{
    public function __construct()
    {

    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identifier', null, ['label' => 'Nif'])
            ->add('name', null, ['label' => 'Nombre'])
            ->add('surname1', null, ['label' => 'Primer Apellido'])
            ->add('surname2', null, ['label' => 'Segundo Apellido'])
            ->add('phone', null, ['label' => 'Tlf'])
            ->add('movil', null, ['label' => 'Móvil'])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CustomerData',
        ));
    }
}
